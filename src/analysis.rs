use crate::{
    angle::AngleInfo,
    errors::*,
    packet::{CompositeHit, PacketizedEvent},
    util::Hit,
};
use datakiste::{
    self,
    cut::{Cut, Cut1d, Cut2d},
    hist::{Hist, Hist1d, Hist2d, Hist3d},
    io::{Datakiste, DkItem},
    points::{Points, Points2d, Points3d, Points4d},
    DetId,
};
use getopts::Options;
use nalgebra::Point2;
use serde_json;
use spade::{
    delaunay::{DelaunayWalkLocate, FloatDelaunayTriangulation},
    HasPosition,
};
use std::{
    any::Any,
    cell::RefCell,
    collections::{HashMap, HashSet},
    env,
    fs::File,
    io::{BufRead, BufReader, BufWriter},
    path::Path,
    process,
};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub angle: Option<String>,
    pub bad_chans: Option<String>,
    pub cut: Option<String>,
    pub thickness: Option<String>,
    pub hists: Option<String>,
    pub sum_hists: Option<String>,
    pub run_info: Option<String>,
    pub kin_line_34s: Option<String>,
    pub kin_line_34cl: Option<String>,
    pub kin_line_34ar: Option<String>,
    pub q_value: Option<String>,
    #[serde(flatten)]
    pub other: HashMap<String, serde_json::Value>,
}

pub enum Reaction {
    S34,
    Cl34,
    Ar34,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RunInfo {
    pub run_energy: String,
    pub start_time: f64,
    pub stop_time: f64,
    pub cap_ic: Option<(f64, f64, f64)>,
    pub cap_in: (f64, f64, f64),
    pub rhoa: (f64, f64, f64),
}

pub struct KinematicPoint {
    pos_e: Point2<f64>,
    q_val: f64,
    theta_cm: f64,
    theta_r: f64,
    e_r: f64,
    ic_x: f64,
    ic_y: f64,
    ic_de: f64,
    ic_e: f64,
}

impl KinematicPoint {
    pub fn new(
        q_val: f64,
        theta_cm: f64,
        theta_e: f64,
        e_e: f64,
        theta_r: f64,
        e_r: f64,
        ic_x: f64,
        ic_y: f64,
        ic_de: f64,
        ic_e: f64,
    ) -> Self {
        KinematicPoint {
            pos_e: Point2::<f64>::new(theta_e, e_e),
            q_val,
            theta_cm,
            theta_r,
            e_r,
            ic_x,
            ic_y,
            ic_de,
            ic_e,
        }
    }

    pub fn q_val(&self) -> f64 {
        self.q_val
    }

    pub fn theta_cm(&self) -> f64 {
        self.theta_cm
    }

    pub fn theta_r(&self) -> f64 {
        self.theta_r
    }

    pub fn e_r(&self) -> f64 {
        self.e_r
    }

    pub fn ic_x(&self) -> f64 {
        self.ic_x
    }

    pub fn ic_y(&self) -> f64 {
        self.ic_y
    }

    pub fn ic_de(&self) -> f64 {
        self.ic_de
    }

    pub fn ic_e(&self) -> f64 {
        self.ic_e
    }
}

impl HasPosition for KinematicPoint {
    type Point = Point2<f64>;
    fn position(&self) -> Point2<f64> {
        self.pos_e
    }
}

pub struct Analysis<'a> {
    config: Config,
    f_run_name: String,
    f_hist_out_name: String,
    f_points_out_name: String,
    pub run_name: String,
    angles: Option<HashMap<DetId, AngleInfo>>,
    bad_chans: Option<HashSet<DetId>>,
    cuts: Option<HashMap<String, Cut>>,
    thick: Option<HashMap<u16, f64>>,
    hists: Option<RefCell<HashMap<String, DkItem<'a>>>>,
    points: RefCell<HashMap<String, DkItem<'a>>>,
    sum_hists: Option<Vec<(String, DkItem<'a>, Vec<String>)>>,
    run_info: Option<HashMap<String, RunInfo>>,
    hit_acceptance_func: Option<Box<dyn Fn(&Analysis, &Hit) -> bool>>,
    start_funcs: Vec<Box<dyn Fn(&Analysis) -> Result<()>>>,
    run_funcs: Vec<Box<dyn Fn(&Analysis, &PacketizedEvent) -> Result<()>>>,
    end_funcs: Vec<Box<dyn Fn(&Analysis) -> Result<()>>>,
    q_vals: Option<Vec<(Reaction, f64, f64)>>,
    kin_lines_34s: Option<FloatDelaunayTriangulation<KinematicPoint, DelaunayWalkLocate>>,
    kin_lines_34cl: Option<FloatDelaunayTriangulation<KinematicPoint, DelaunayWalkLocate>>,
    kin_lines_34ar: Option<FloatDelaunayTriangulation<KinematicPoint, DelaunayWalkLocate>>,
    pub other_data: RefCell<HashMap<String, Box<dyn Any>>>,
}

impl<'a> Analysis<'a> {
    //
    // Constructors
    //

    pub fn from_args() -> Result<Analysis<'a>> {
        // Parse the command line arguments
        let opts = Options::new();
        let args: Vec<String> = env::args().collect();
        let matches = opts.parse(&args[1..])?;
        if matches.free.len() != 2 {
            eprintln!("Usage: <program_name> [run_file] [config_file]");
            process::exit(1)
        }

        let f_config = File::open(&matches.free[1]).chain_err(|| "failed to open config file")?;
        let f_config = BufReader::new(f_config);
        let config: Config =
            serde_json::from_reader(f_config).chain_err(|| "failed to read config file")?;

        // Create filenames
        let f_run_name = &matches.free[0];
        let base = Path::new(&f_run_name);
        let base = base.file_stem().ok_or_else(|| "failed to get file stem")?;
        let base = base
            .to_str()
            .ok_or_else(|| "failed to convert base to str")?
            .to_string();
        let f_hist_out_name = &format!("{}.dkh", base);
        let f_points_out_name = &format!("{}.dkp", base);

        Ok(Analysis {
            config,
            f_run_name: f_run_name.to_string(),
            f_hist_out_name: f_hist_out_name.to_string(),
            f_points_out_name: f_points_out_name.to_string(),
            run_name: String::new(),
            angles: None,
            bad_chans: None,
            cuts: None,
            thick: None,
            hists: None,
            sum_hists: None,
            points: RefCell::new(HashMap::new()),
            run_info: None,
            hit_acceptance_func: None,
            start_funcs: Vec::new(),
            run_funcs: Vec::new(),
            end_funcs: Vec::new(),
            q_vals: None,
            kin_lines_34s: None,
            kin_lines_34cl: None,
            kin_lines_34ar: None,
            other_data: RefCell::new(HashMap::new()),
        })
    }

    //
    // Input Functions
    //

    fn read_angle_file(&mut self) -> Result<HashMap<DetId, AngleInfo>> {
        let mut angles = HashMap::new();

        let f_angle_name = self
            .config
            .angle
            .as_ref()
            .ok_or_else(|| "angle file not supplied in config")?;
        let f_angle = File::open(f_angle_name)?;
        let f_angle = BufReader::new(f_angle);

        for l in f_angle.lines() {
            let l = l?;
            let x: Vec<_> = l.split_whitespace().collect();
            if x.is_empty() || x[0].starts_with('#') {
                continue;
            } else if x.len() < 8 {
                eprintln!("WARNING: Error parsing a line in the angle file.");
            } else {
                let detid = x[0].parse::<u16>()?;
                let detch = x[1].parse::<u16>()?;

                let theta_min = x[2].parse::<f64>()?;
                let theta_max = x[3].parse::<f64>()?;
                let theta_avg = x[4].parse::<f64>()?;

                let phi_min = x[5].parse::<f64>()?;
                let phi_max = x[6].parse::<f64>()?;
                let phi_avg = x[7].parse::<f64>()?;

                let solid_angle = x[8].parse::<f64>()?;

                let v = angles.insert(
                    DetId(detid, detch),
                    AngleInfo {
                        theta_min,
                        theta_max,
                        theta_avg,
                        phi_min,
                        phi_max,
                        phi_avg,
                        solid_angle,
                    },
                );
                if v.is_some() {
                    eprintln!(
                        "WARNING: There is already angle info for Det ID ({}, {}).",
                        detid, detch
                    );
                }
            }
        }

        Ok(angles)
    }

    fn read_bad_chans_file(&mut self) -> Result<HashSet<DetId>> {
        let f_bad_chans_name = self
            .config
            .bad_chans
            .as_ref()
            .ok_or_else(|| "bad_chans file not supplied in config")?;
        let f_bad_chans = BufReader::new(File::open(f_bad_chans_name)?);

        Ok(serde_json::from_reader(f_bad_chans)?)
    }

    fn read_cut_file(&mut self) -> Result<HashMap<String, Cut>> {
        let f_cut_name = self
            .config
            .cut
            .as_ref()
            .ok_or_else(|| "cut file not supplied in config")?;
        let f_cut = BufReader::new(File::open(f_cut_name)?);

        Ok(serde_json::from_reader(f_cut)?)
    }

    fn read_thick_file(&mut self) -> Result<HashMap<u16, f64>> {
        let mut thicks = HashMap::new();

        let f_thick_name = self
            .config
            .thickness
            .as_ref()
            .ok_or_else(|| "thickness file not supplied in config")?;
        let f_thick = File::open(f_thick_name)?;
        let f_thick = BufReader::new(f_thick);

        for l in f_thick.lines() {
            let l = l?;
            let x: Vec<_> = l.split_whitespace().collect();
            if x.is_empty() || x[0].starts_with('#') {
                continue;
            } else if x.len() < 2 {
                eprintln!("WARNING: Error parsing a line in the thickness file.");
            } else {
                let detid = x[0].parse::<u16>()?;
                let thick = x[1].parse::<f64>()?;

                let v = thicks.insert(detid, thick);
                if v.is_some() {
                    eprintln!(
                        "WARNING: There is already a thickness for Det ID {}.",
                        detid
                    );
                }
            }
        }

        Ok(thicks)
    }

    fn read_hists_file(&mut self) -> Result<HashMap<String, DkItem<'a>>> {
        let mut hists = HashMap::new();

        let f_hists_name = self
            .config
            .hists
            .as_ref()
            .ok_or_else(|| "histogram file not supplied in config")?;
        let f_hists = File::open(f_hists_name)?;
        let f_hists = BufReader::new(f_hists);

        for line in f_hists.lines() {
            let l = line?;
            let x: Vec<_> = l.split_whitespace().collect();
            if x.len() == 4 {
                let name = x[0];
                let bins = x[1].parse::<u32>()?;
                let min = x[2].parse::<f64>()?;
                let max = x[3].parse::<f64>()?;
                hists.insert(
                    name.to_string(),
                    Hist1d::new(bins, min, max)
                        .ok_or_else(|| "failed to make Hist1d")?
                        .into(),
                );
            } else if x.len() == 7 {
                let name = x[0];
                let bins_x = x[1].parse::<u32>()?;
                let min_x = x[2].parse::<f64>()?;
                let max_x = x[3].parse::<f64>()?;
                let bins_y = x[4].parse::<u32>()?;
                let min_y = x[5].parse::<f64>()?;
                let max_y = x[6].parse::<f64>()?;
                hists.insert(
                    name.to_string(),
                    Hist2d::new(bins_x, min_x, max_x, bins_y, min_y, max_y)
                        .ok_or_else(|| "failed to make Hist2d")?
                        .into(),
                );
            } else if x.len() == 10 {
                let name = x[0];
                let bins_1 = x[1].parse::<u32>()?;
                let min_1 = x[2].parse::<f64>()?;
                let max_1 = x[3].parse::<f64>()?;
                let bins_2 = x[4].parse::<u32>()?;
                let min_2 = x[5].parse::<f64>()?;
                let max_2 = x[6].parse::<f64>()?;
                let bins_3 = x[7].parse::<u32>()?;
                let min_3 = x[8].parse::<f64>()?;
                let max_3 = x[9].parse::<f64>()?;
                hists.insert(
                    name.to_string(),
                    Hist3d::new(
                        bins_1, min_1, max_1, bins_2, min_2, max_2, bins_3, min_3, max_3,
                    )
                    .ok_or_else(|| "failed to make Hist3d")?
                    .into(),
                );
            } else {
                eprintln!("WARNING: Error parsing a line in the histogram file.");
            }
        }

        Ok(hists)
    }

    fn read_sum_hists_file(&mut self) -> Result<Vec<(String, DkItem<'a>, Vec<String>)>> {
        let mut sum_hists = Vec::new();

        enum State<'b> {
            Hist,
            Sum(String, DkItem<'b>),
        }
        let mut s = State::Hist;

        let f_sum_hists_name = self
            .config
            .sum_hists
            .as_ref()
            .ok_or_else(|| "sum histogram file not supplied in config")?;
        let f_sum_hists = File::open(f_sum_hists_name)?;
        let f_sum_hists = BufReader::new(f_sum_hists);

        for line in f_sum_hists.lines() {
            let l = line?;
            let x: Vec<_> = l.split_whitespace().collect();
            match s {
                State::Hist => {
                    if x.len() == 4 {
                        let name = x[0];
                        let bins = x[1].parse::<u32>()?;
                        let min = x[2].parse::<f64>()?;
                        let max = x[3].parse::<f64>()?;

                        s = State::Sum(
                            name.to_string(),
                            Hist1d::new(bins, min, max)
                                .ok_or_else(|| "failed to make Hist1d")?
                                .into(),
                        );
                    } else if x.len() == 7 {
                        let name = x[0];
                        let bins_x = x[1].parse::<u32>()?;
                        let min_x = x[2].parse::<f64>()?;
                        let max_x = x[3].parse::<f64>()?;
                        let bins_y = x[4].parse::<u32>()?;
                        let min_y = x[5].parse::<f64>()?;
                        let max_y = x[6].parse::<f64>()?;

                        s = State::Sum(
                            name.to_string(),
                            Hist2d::new(bins_x, min_x, max_x, bins_y, min_y, max_y)
                                .ok_or_else(|| "failed to make Hist2d")?
                                .into(),
                        );
                    } else {
                        eprintln!("WARNING: Error parsing a line in the sum histogram file.");
                    }
                }
                State::Sum(name, hist) => {
                    let mut v = Vec::<String>::new();
                    for i in x {
                        v.push(i.to_string());
                    }
                    sum_hists.push((name, hist, v));
                    s = State::Hist;
                }
            }
        }

        Ok(sum_hists)
    }

    fn read_run_info_file(&self) -> Result<HashMap<String, RunInfo>> {
        let f_run_info_name = self
            .config
            .run_info
            .as_ref()
            .ok_or_else(|| "run info file not supplied in config")?;
        let f_run_info = File::open(f_run_info_name)?;
        let f_run_info = BufReader::new(f_run_info);

        Ok(serde_json::from_reader(f_run_info)?)
    }

    fn read_kin_lines_file(
        &self,
        f_name: &str,
    ) -> Result<FloatDelaunayTriangulation<KinematicPoint, DelaunayWalkLocate>> {
        let f_kin_lines = File::open(f_name)?;
        let f_kin_lines = BufReader::new(f_kin_lines);

        let mut t = FloatDelaunayTriangulation::with_walk_locate();

        for line in f_kin_lines.lines() {
            let l = line?;
            let x: Vec<_> = l.split_whitespace().collect();

            if x.is_empty() || x[0].starts_with('#') {
                // Ignore comments and blank lines
                continue;
            } else {
                // Build current kinematic line
                let q_val = x[0].parse::<f64>()? * 1000.0;
                let th_cm = x[1].parse::<f64>()?;
                let th_e = x[2].parse::<f64>()?;
                let e_e = x[3].parse::<f64>()? * 1000.0;
                let th_r = x[5].parse::<f64>()?;
                let e_r = x[6].parse::<f64>()? * 1000.0;
                let ic_x = x[7].parse::<f64>()?;
                let ic_y = x[8].parse::<f64>()?;
                let ic_de = x[9].parse::<f64>()?;
                let ic_e = x[10].parse::<f64>()?;

                t.insert(KinematicPoint::new(
                    q_val, th_cm, th_e, e_e, th_r, e_r, ic_x, ic_y, ic_de, ic_e,
                ));
            }
        }

        Ok(t)
    }

    fn read_q_val_file(&mut self) -> Result<()> {
        let f_q_val_name = self
            .config
            .q_value
            .as_ref()
            .ok_or_else(|| "q value file not supplied in config")?;
        let f_q_val = File::open(f_q_val_name)?;
        let f_q_val = BufReader::new(f_q_val);

        let mut q_vals = Vec::new();

        for line in f_q_val.lines() {
            let l = line?;
            let x: Vec<_> = l.split_whitespace().collect();

            if x.is_empty() || x[0].starts_with('#') {
                // Ignore comments and blank lines
                continue;
            } else {
                let r = match x[0] {
                    "34S" => Ok(Reaction::S34),
                    "34Cl" => Ok(Reaction::Cl34),
                    "34Ar" => Ok(Reaction::Ar34),
                    _ => Err("Could not parse q value line"),
                }?;
                let q = x[1].parse::<f64>()? * 1000.0;
                let sigma = x[2].parse::<f64>()?;

                q_vals.push((r, q, sigma));
            }
        }

        self.q_vals = Some(q_vals);

        Ok(())
    }

    //
    // Analysis Function
    //

    pub fn run(&mut self) -> Result<()> {
        if self.config.angle.as_ref().is_some() {
            self.angles = Some(
                self.read_angle_file()
                    .chain_err(|| "failed to read angle file")?,
            );
        }
        if self.config.bad_chans.as_ref().is_some() {
            self.bad_chans = Some(
                self.read_bad_chans_file()
                    .chain_err(|| "failed to read bad_chans file")?,
            );
        }
        if self.config.cut.as_ref().is_some() {
            self.cuts = Some(
                self.read_cut_file()
                    .chain_err(|| "failed to read cut file")?,
            );
        }
        if self.config.thickness.as_ref().is_some() {
            self.thick = Some(
                self.read_thick_file()
                    .chain_err(|| "failed to read thickness file")?,
            );
        }
        if self.config.hists.as_ref().is_some() {
            self.hists = Some(RefCell::new(
                self.read_hists_file()
                    .chain_err(|| "failed to read hist file")?,
            ));
        }
        if self.config.sum_hists.as_ref().is_some() {
            self.sum_hists = Some(
                self.read_sum_hists_file()
                    .chain_err(|| "failed to read sum hist file")?,
            );
        }
        if self.config.run_info.as_ref().is_some() {
            self.run_info = Some(
                self.read_run_info_file()
                    .chain_err(|| "failed to read run info file")?,
            );
        }
        if let Some(f_name) = self.config.kin_line_34s.as_ref() {
            self.kin_lines_34s = Some(
                self.read_kin_lines_file(&f_name)
                    .chain_err(|| "failed to read 34S kinematic line file")?,
            );
        }
        if let Some(f_name) = self.config.kin_line_34cl.as_ref() {
            self.kin_lines_34cl = Some(
                self.read_kin_lines_file(&f_name)
                    .chain_err(|| "failed to read 34Cl kinematic line file")?,
            );
        }
        if let Some(f_name) = self.config.kin_line_34ar.as_ref() {
            self.kin_lines_34ar = Some(
                self.read_kin_lines_file(&f_name)
                    .chain_err(|| "failed to read 34Ar kinematic line file")?,
            );
        }
        if self.config.q_value.is_some() {
            self.read_q_val_file()
                .chain_err(|| "failed to read q value file")?;
        }

        // Read in run from input file
        let f_run =
            BufReader::new(File::open(&self.f_run_name).chain_err(|| "failed to open run file")?);

        for (i, f) in self.start_funcs.iter().enumerate() {
            f(&self).chain_err(|| format!("failed to run start function {}", i))?;
        }

        // Main Loop
        let dk: Datakiste = bincode::deserialize_from(f_run)?;
        for (n, item) in dk {
            if let Some(run) = item.into_run() {
                self.run_name = n;

                for e in run.events {
                    let pack = PacketizedEvent::new(e, self, &self.hit_acceptance_func)?;

                    for (i, f) in self.run_funcs.iter().enumerate() {
                        f(&self, &pack)
                            .chain_err(|| format!("failed to run analysis function {}", i))?;
                    }
                }
            }
        }

        self.sum_spectra().chain_err(|| "failed to sum spectra")?;
        self.output().chain_err(|| "failed to output spectra")?;

        for (i, f) in self.end_funcs.iter().enumerate() {
            f(&self).chain_err(|| format!("failed to run end function {}", i))?;
        }

        Ok(())
    }

    pub fn register_acceptance_func(
        &mut self,
        f: Box<dyn Fn(&Analysis, &Hit) -> bool>,
    ) -> Result<()> {
        if self.hit_acceptance_func.is_some() {
            bail!("hit_acceptance_func already exists");
        }

        self.hit_acceptance_func = Some(f);
        Ok(())
    }

    pub fn register_start_func(&mut self, f: Box<dyn Fn(&Analysis) -> Result<()>>) -> Result<()> {
        self.start_funcs.push(f);
        Ok(())
    }

    pub fn register_run_func(
        &mut self,
        f: Box<dyn Fn(&Analysis, &PacketizedEvent) -> Result<()>>,
    ) -> Result<()> {
        self.run_funcs.push(f);
        Ok(())
    }

    pub fn register_end_func(&mut self, f: Box<dyn Fn(&Analysis) -> Result<()>>) -> Result<()> {
        self.end_funcs.push(f);
        Ok(())
    }

    // Recoils
    pub fn ic_37k_corr(
        &self,
        x: Option<f64>,
        y: Option<f64>,
        de: Option<f64>,
        e: Option<f64>,
    ) -> Result<(Option<f64>, Option<f64>, Option<f64>, Option<f64>)> {
        let p = self
            .get_run_info(&self.run_name)?
            .cap_ic
            .ok_or(ErrorKind::MissingRunInfo(self.run_name.clone()))?
            .0;
        let x_a = 0.425_156_25;
        let y_a = 0.387_468_75;
        let de_a = 0.383_875;
        let e_a = -1.429_968_75;

        let x_b = 0.040_072_917;
        let y_b = 0.470_502_083;
        let de_b = 5.521_15;
        let e_b = 28.472_239_58;

        Ok((
            x.map(|x| (x_a * 15.0 + x_b) / (x_a * p + x_b) * x),
            y.map(|y| (y_a * 15.0 + y_b) / (y_a * p + y_b) * y),
            de.map(|de| (de_a * 15.0 + de_b) / (de_a * p + de_b) * de),
            e.map(|e| (e_a * 15.0 + e_b) / (e_a * p + e_b) * e),
        ))
    }

    pub fn ic_37ar_corr(
        &self,
        x: Option<f64>,
        y: Option<f64>,
        de: Option<f64>,
        e: Option<f64>,
    ) -> Result<(Option<f64>, Option<f64>, Option<f64>, Option<f64>)> {
        let p = self
            .get_run_info(&self.run_name)?
            .cap_ic
            .ok_or(ErrorKind::MissingRunInfo(self.run_name.clone()))?
            .0;
        let x_a = 0.403_718_75;
        let y_a = 0.379_437_499_999_999;
        let de_a = 0.494_656_25;
        let e_a = -1.498_406_25;

        let x_b = 0.013_643_750_000_005;
        let y_b = 0.302_154_166_666_68;
        let de_b = 3.720_331_250_000_01;
        let e_b = 31.331_318_75;

        Ok((
            x.map(|x| (x_a * 15.0 + x_b) / (x_a * p + x_b) * x),
            y.map(|y| (y_a * 15.0 + y_b) / (y_a * p + y_b) * y),
            de.map(|de| (de_a * 15.0 + de_b) / (de_a * p + de_b) * de),
            e.map(|e| (e_a * 15.0 + e_b) / (e_a * p + e_b) * e),
        ))
    }

    pub fn ic_37cl_corr(
        &self,
        x: Option<f64>,
        y: Option<f64>,
        de: Option<f64>,
        e: Option<f64>,
    ) -> Result<(Option<f64>, Option<f64>, Option<f64>, Option<f64>)> {
        let p = self
            .get_run_info(&self.run_name)?
            .cap_ic
            .ok_or(ErrorKind::MissingRunInfo(self.run_name.clone()))?
            .0;
        let x_a = 0.382_718_75;
        let y_a = 0.388_968_75;
        let de_a = 0.570_625;
        let e_a = -1.524_031_25;

        let x_b = -0.037_489_583_333_326;
        let y_b = -0.158_472_916_666_662;
        let de_b = 2.261_708_333_333_34;
        let e_b = 33.808_493_75;

        Ok((
            x.map(|x| (x_a * 15.0 + x_b) / (x_a * p + x_b) * x),
            y.map(|y| (y_a * 15.0 + y_b) / (y_a * p + y_b) * y),
            de.map(|de| (de_a * 15.0 + de_b) / (de_a * p + de_b) * de),
            e.map(|e| (e_a * 15.0 + e_b) / (e_a * p + e_b) * e),
        ))
    }

    // Beam
    pub fn ic_34ar_corr(
        &self,
        x: Option<f64>,
        y: Option<f64>,
        de: Option<f64>,
        e: Option<f64>,
    ) -> Result<(Option<f64>, Option<f64>, Option<f64>, Option<f64>)> {
        let p = self
            .get_run_info(&self.run_name)?
            .cap_ic
            .ok_or(ErrorKind::MissingRunInfo(self.run_name.clone()))?
            .0;
        let x_a = 0.393_875;
        let y_a = 0.360_479_167;
        let de_a = 0.224_145_833;
        let e_a = -1.197_854_167;

        let x_b = 0.084_275;
        let y_b = 0.450_718_056;
        let de_b = 6.666_873_611;
        let e_b = 24.254_623_61;

        Ok((
            x.map(|x| (x_a * 15.0 + x_b) / (x_a * p + x_b) * x),
            y.map(|y| (y_a * 15.0 + y_b) / (y_a * p + y_b) * y),
            de.map(|de| (de_a * 15.0 + de_b) / (de_a * p + de_b) * de),
            e.map(|e| (e_a * 15.0 + e_b) / (e_a * p + e_b) * e),
        ))
    }

    pub fn si_thick_corr(&self, ch_de: &CompositeHit, ch_e: &CompositeHit) -> Result<(f64, f64)> {
        let thickness = self
            .thick
            .as_ref()
            .ok_or_else(|| "thick does not exist")?
            .get(&ch_de.detnum)
            .ok_or_else(|| format!("no thickness data for {}", ch_de.detnum))?;
        let de60 = ch_de.energy * 60.0 / thickness;
        let e60 = ch_e.energy + (ch_de.energy - de60);

        Ok((de60, e60))
    }

    //
    // Getter Functions
    //

    pub fn contains_hist(&self, name: &str) -> bool {
        self.hists
            .as_ref()
            .map_or_else(|| false, |h| h.borrow().contains_key(name))
    }

    pub fn get_config(&self) -> &Config {
        &self.config
    }

    pub fn get_bad_chans(&self) -> Result<&HashSet<DetId>> {
        Ok(self
            .bad_chans
            .as_ref()
            .ok_or_else(|| "bad_chans does not exist")?)
    }

    pub fn get_run_info(&self, name: &str) -> Result<&RunInfo> {
        Ok(self
            .run_info
            .as_ref()
            .map(|ri| {
                ri.get(name)
                    .ok_or_else(|| format!("'{}' does not exist in run_info", name))
            })
            .ok_or_else(|| "run_info does not exist")??)
    }

    pub fn get_cut_1d(&self, name: &str) -> Result<&Cut1d> {
        let cut = self
            .cuts
            .as_ref()
            .ok_or_else(|| "cuts does not exist")?
            .get(name)
            .ok_or_else(|| format!("'{}' does not exist in cuts", name))?;

        match cut {
            Cut::Cut1d(c) => Ok(c),
            _ => Err(format!("'{}' is not a Cut1d", name))?,
        }
    }

    pub fn get_cut_2d(&self, name: &str) -> Result<&Cut2d> {
        let cut = self
            .cuts
            .as_ref()
            .ok_or_else(|| "cuts does not exist")?
            .get(name)
            .ok_or_else(|| format!("'{}' does not exist in cuts", name))?;

        match cut {
            Cut::Cut2d(c) => Ok(c),
            _ => Err(format!("'{}' is not a Cut2d", name))?,
        }
    }

    pub fn get_all_angle_info(&self) -> Result<&HashMap<DetId, AngleInfo>> {
        Ok(self
            .angles
            .as_ref()
            .ok_or_else(|| "angles does not exist")?)
    }

    pub fn get_angle_info(&self, detnum: u16, detch: f64) -> Result<AngleInfo> {
        if let Some(a) = self
            .angles
            .as_ref()
            .ok_or_else(|| "angles does not exist")?
            .get(&DetId(detnum, detch.round() as u16))
        {
            Ok(a.clone())
        } else {
            bail!("no angle data for ({}, {})", detnum, detch)
        }
        /*
        let d1 = DetId(detnum, detch.floor_else(|| ) as u16);
        let d2 = DetId(detnum, detch.ceil() as u16);

        match (self.angles.get(&d1), self.angles.get(&d2)) {
            (None, None) => bail!("no angle data for ({}, {})", detnum, detch),
            (Some(a), None) | (None, Some(a)) => Ok(a.clone()),
            (Some(a1), Some(a2)) => {
                Ok(AngleInfo {
                       theta_min: f64::min(a1.theta_min, a2.theta_min),
                       theta_max: f64::max(a1.theta_max, a2.theta_max),
                       theta_avg: (a2.theta_avg - a1.theta_avg) / ((d2.1 as f64) - (d1.1 as f64)) *
                                  (detch - (d1.1 as f64)) +
                                  a1.theta_avg,
                       phi_min: f64::min(a1.phi_min, a2.phi_min),
                       phi_max: f64::max(a1.phi_max, a2.phi_max),
                       phi_avg: (a2.phi_avg - a1.phi_avg) / ((d2.1 as f64) - (d1.1 as f64)) *
                                (detch - (d1.1 as f64)) +
                                a1.phi_avg,
                   })
            }
        }
        */
    }

    pub fn get_q_vals(&self) -> Result<&Vec<(Reaction, f64, f64)>> {
        Ok(self
            .q_vals
            .as_ref()
            .ok_or_else(|| "q_vals does not exist")?)
    }

    pub fn get_q_val_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::q_val)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_q_val_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::q_val)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_q_val_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::q_val)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_th_cm_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_cm)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_th_cm_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_cm)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_th_cm_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_cm)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_th_r_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_th_r_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_th_r_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::theta_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_e_r_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::e_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_e_r_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::e_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_e_r_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::e_r)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_ic_x_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_x)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_ic_x_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_x)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_ic_x_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_x)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_ic_y_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_y)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_ic_y_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_y)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_ic_y_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_y)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_ic_de_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_de)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_ic_de_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_de)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_ic_de_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_de)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    pub fn get_ic_e_34s(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34s.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_e)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34s does not exist")
        }
    }

    pub fn get_ic_e_34cl(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34cl.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_e)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34cl does not exist")
        }
    }

    pub fn get_ic_e_34ar(&self, x: f64, y: f64) -> Result<f64> {
        if let Some(k) = self.kin_lines_34ar.as_ref() {
            Ok(
                k.nn_interpolation(&Point2::<f64>::new(x, y), KinematicPoint::ic_e)
                    .ok_or_else(|| "problem interpolating")?,
            )
        } else {
            bail!("kin_lines_34ar does not exist")
        }
    }

    //
    // Setter Functions
    //

    pub fn fill_1d(&self, name: &str, val: f64) -> Result<()> {
        let mut hists = self
            .hists
            .as_ref()
            .ok_or_else(|| "hists does not exist")?
            .borrow_mut();
        let hist = hists
            .get_mut(name)
            .ok_or_else(|| format!("'{}' does not exist in hists", name))?
            .as_hist_1d_mut()
            .ok_or_else(|| format!("'{}' is not a Hist1d", name))?;
        hist.fill(val);
        Ok(())
    }

    pub fn fill_2d(&self, name: &str, val_x: f64, val_y: f64) -> Result<()> {
        let mut hists = self
            .hists
            .as_ref()
            .ok_or_else(|| "hists does not exist")?
            .borrow_mut();
        let hist = hists
            .get_mut(name)
            .ok_or_else(|| format!("'{}' does not exist in hists", name))?
            .as_hist_2d_mut()
            .ok_or_else(|| format!("'{}' is not a Hist2d", name))?;
        hist.fill((val_x, val_y));

        Ok(())
    }

    pub fn push_2d(&self, name: &str, val_x: f64, val_y: f64) -> Result<()> {
        let mut points = self.points.borrow_mut();
        let p = points
            .entry(name.to_string())
            .or_insert_with(|| Points2d::new().into())
            .as_points_2d_mut()
            .ok_or_else(|| format!("'{}' is not a Points2d", name))?;
        p.push((val_x, val_y));

        Ok(())
    }

    pub fn fill_3d(&self, name: &str, val_1: f64, val_2: f64, val_3: f64) -> Result<()> {
        let mut hists = self
            .hists
            .as_ref()
            .ok_or_else(|| "hists does not exist")?
            .borrow_mut();
        let hist = hists
            .get_mut(name)
            .ok_or_else(|| format!("'{}' does not exist in hists", name))?
            .as_hist_3d_mut()
            .ok_or_else(|| format!("'{}' is not a Hist3d", name))?;
        hist.fill((val_1, val_2, val_3));

        Ok(())
    }

    pub fn push_3d(&self, name: &str, val_1: f64, val_2: f64, val_3: f64) -> Result<()> {
        let mut points = self.points.borrow_mut();
        let p = points
            .entry(name.to_string())
            .or_insert_with(|| Points3d::new().into())
            .as_points_3d_mut()
            .ok_or_else(|| format!("'{}' is not a Points3d", name))?;
        p.push((val_1, val_2, val_3));

        Ok(())
    }

    pub fn push_4d(
        &self,
        name: &str,
        val_1: f64,
        val_2: f64,
        val_3: f64,
        val_4: f64,
    ) -> Result<()> {
        let mut points = self.points.borrow_mut();
        let p = points
            .entry(name.to_string())
            .or_insert_with(|| Points4d::new().into())
            .as_points_4d_mut()
            .ok_or_else(|| format!("'{}' is not a Points4d", name))?;
        p.push((val_1, val_2, val_3, val_4));

        Ok(())
    }

    //
    // Output Functions
    //

    pub fn sum_spectra(&mut self) -> Result<()> {
        let mut sums = None;
        ::std::mem::swap(&mut sums, &mut self.sum_hists);
        if let Some(sums) = sums {
            for (n1, mut h1, v) in sums {
                match h1 {
                    DkItem::Hist1d(_) => {
                        let h1_inner = h1
                            .as_hist_1d_mut()
                            .ok_or_else(|| format!("'{}' is not a Hist1d", n1))?;
                        for n2 in v {
                            let hists = self
                                .hists
                                .as_ref()
                                .ok_or_else(|| "hists does not exist")?
                                .borrow();
                            let h2 = hists
                                .get(&n2)
                                .ok_or_else(|| format!("'{}' does not exist in hists", n2))?
                                .as_hist_1d()
                                .ok_or_else(|| format!("'{}' is not a Hist1d", n2))?;
                            h1_inner.add(h2);
                        }
                        self.hists
                            .as_ref()
                            .ok_or_else(|| "hists does not exist")?
                            .borrow_mut()
                            .insert(n1, h1);
                    }
                    DkItem::Hist2d(_) => {
                        let h1_inner = h1
                            .as_hist_2d_mut()
                            .ok_or_else(|| format!("'{}' is not a Hist2d", n1))?;
                        for n2 in v {
                            let hists = self
                                .hists
                                .as_ref()
                                .ok_or_else(|| "hists does not exist")?
                                .borrow();
                            let h2 = hists
                                .get(&n2)
                                .ok_or_else(|| format!("'{}' does not exist in hists", n2))?
                                .as_hist_2d()
                                .ok_or_else(|| format!("'{}' is not a Hist2d", n2))?;
                            h1_inner.add(h2);
                        }
                        self.hists
                            .as_ref()
                            .ok_or_else(|| "hists does not exist")?
                            .borrow_mut()
                            .insert(n1, h1);
                    }
                    _ => {}
                }
            }
        }

        Ok(())
    }

    pub fn output(&mut self) -> Result<()> {
        if let Some(hists) = self.hists.take() {
            if !hists.borrow().is_empty() {
                let items = hists.into_inner().into_iter().collect();
                let dk = Datakiste::with_items(items);

                let f_hist_out = BufWriter::new(File::create(&self.f_hist_out_name)?);
                bincode::serialize_into(f_hist_out, &dk)?;
            }
        }

        if !self.points.borrow().is_empty() {
            let items = self
                .points
                .borrow()
                .iter()
                .map(|(a, b)| (a.clone(), b.clone()))
                .collect();
            let dk = Datakiste::with_items(items);

            let f_points_out = BufWriter::new(File::create(&self.f_points_out_name)?);
            bincode::serialize_into(f_points_out, &dk)?;
        }

        Ok(())
    }
}
