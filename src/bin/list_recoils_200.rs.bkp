use e15232_analysis::{
    analysis_200::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{output::Output, Hit},
};
use std::f64::consts::FRAC_1_PI;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.register_acceptance_func(Box::new(accept))?;
    analysis.run()?;
    Ok(())
}

fn accept(h: &Hit) -> bool {
    match h.detid.0 {
        1..=42 => !(h.value == 0 || h.value > 16000),
        43 => false,
        _ => true,
    }
}

#[allow(non_snake_case)]
fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_ic in &pack.ic {
        let ic_dist_z_x = 558.8 + 20. + 18.3;
        let ic_dist_z_y = ic_dist_z_x + 36.6;
        let ic_pos_center_x = 16.597;
        let ic_pos_center_y = 15.000;

        let ic_e_x = p_ic.comp_x().map(|ch| ch.energy);
        let ic_e_y = p_ic.comp_y().map(|ch| ch.energy);
        let ic_e_de = p_ic.comp_de().map(|ch| ch.energy);
        let ic_e_e = p_ic.comp_e().map(|ch| ch.energy);
        let ic_pos_x = p_ic.comp_x().map(|ch| ch.detch);
        let ic_pos_y = p_ic.comp_y().map(|ch| ch.detch);
        let ic_dist_x = ic_pos_x.map(|p| (p - ic_pos_center_x) * 3.0);
        let ic_dist_y = ic_pos_y.map(|p| (p - ic_pos_center_y) * 3.0);
        let ic_theta = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
            Some(
                180. * FRAC_1_PI
                    * f64::atan(f64::sqrt(
                        (d_x * d_x) / (ic_dist_z_x * ic_dist_z_x)
                            + (d_y * d_y) / (ic_dist_z_y * ic_dist_z_y),
                    )),
            )
        } else {
            None
        };
        let ic_phi = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
            Some(180. * FRAC_1_PI * f64::atan2(-d_y, d_x))
        } else {
            None
        };

        let si_e_de: Option<f64> = None;
        let si_e_e: Option<f64> = None;
        let si_theta: Option<f64> = None;
        let _si_phi_ch: Option<f64> = None;
        let si_phi: Option<f64> = None;

        let out = Output::new()
            .run_name(a.run_name.clone())
            .run_pressure(a.get_run_info(&a.run_name)?.pressure)
            .ic_x_energy(ic_e_x)
            .ic_y_energy(ic_e_y)
            .ic_de_energy(ic_e_de)
            .ic_e_energy(ic_e_e)
            .ic_pos_x(ic_pos_x)
            .ic_pos_y(ic_pos_y)
            .ic_theta(ic_theta)
            .ic_phi(ic_phi)
            .si_de_energy(si_e_de)
            .si_e_energy(si_e_e)
            .si_theta(si_theta)
            .si_phi(si_phi);
        println!("{}", out);
    }

    Ok(())
}
