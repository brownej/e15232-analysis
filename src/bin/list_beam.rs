use e15232_analysis::{
    analysis::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{
        ic_angles,
        output::{print_formatted, Data, Output},
        Hit,
    },
};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(add_output))?;
    analysis.register_start_func(Box::new(add_counter))?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.register_acceptance_func(Box::new(accept))?;
    analysis.register_end_func(Box::new(print_output))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().thickness.is_none() {
        Err("thickness file not supplied in config")?
    }
    if a.get_config().bad_chans.is_none() {
        Err("bad_chans file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().run_info.is_none() {
        Err("run_info file not supplied in config")?
    }

    Ok(())
}

fn add_output(a: &Analysis) -> Result<()> {
    a.other_data
        .borrow_mut()
        .insert("output".to_string(), Box::new(Output::new()));
    Ok(())
}

fn add_counter(a: &Analysis) -> Result<()> {
    a.other_data
        .borrow_mut()
        .insert("counter".to_string(), Box::new(0u32));
    Ok(())
}

fn print_output(a: &Analysis) -> Result<()> {
    print_formatted(
        a.other_data
            .borrow()
            .get("output")
            .ok_or("output not in other_data")?
            .downcast_ref::<Output>()
            .ok_or("output is not an Output")?,
    )?;
    println!();
    Ok(())
}

fn accept(a: &Analysis, h: &Hit) -> bool {
    if a.get_bad_chans().unwrap().contains(&h.detid) {
        return false;
    }
    match h.detid.0 {
        1..=42 => !(h.value == 0 || h.value > 16000),
        43 => false,
        _ => true,
    }
}

#[allow(non_snake_case)]
fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    // only go through the first 10000 events
    {
        let mut other_data = a.other_data.borrow_mut();
        let counter = other_data
            .get_mut("counter")
            .ok_or("counter not in other_data")?
            .downcast_mut::<u32>()
            .ok_or("counter is not a u32")?;
        *counter += 1;
        if *counter >= 10000 {
            return Ok(());
        }
    }

    let mut other_data = a.other_data.borrow_mut();
    let output = other_data
        .get_mut("output")
        .ok_or("output not in other_data")?
        .downcast_mut::<Output>()
        .ok_or("output is not an Output")?;

    for p_ic in &pack.ic {
        let ic_e_x = p_ic.comp_x().map(|ch| ch.energy);
        let ic_e_y = p_ic.comp_y().map(|ch| ch.energy);
        let ic_e_de = p_ic.comp_de().map(|ch| ch.energy);
        let ic_e_e = p_ic.comp_e().map(|ch| ch.energy);
        let ic_e_tot = ic_e_x.unwrap_or(0.0)
            + ic_e_y.unwrap_or(0.0)
            + ic_e_de.unwrap_or(0.0)
            + ic_e_e.unwrap_or(0.0);
        let ic_pos_x = p_ic.comp_x().map(|ch| ch.detch);
        let ic_pos_y = p_ic.comp_y().map(|ch| ch.detch);
        let (ic_theta, ic_phi) = if let (Some(p_x), Some(p_y)) = (ic_pos_x, ic_pos_y) {
            let angles = ic_angles(p_x, p_y);
            (Some(angles.0), Some(angles.1))
        } else {
            (None, None)
        };

        let ic_x_chan = {
            let chan: Vec<_> = p_ic
                .all_x()
                .iter()
                .map(|h| (h.detid.1, h.value as f64))
                .collect();
            if !chan.is_empty() {
                Some(chan)
            } else {
                None
            }
        };
        let ic_y_chan = {
            let chan: Vec<_> = p_ic
                .all_y()
                .iter()
                .map(|h| (h.detid.1, h.value as f64))
                .collect();
            if !chan.is_empty() {
                Some(chan)
            } else {
                None
            }
        };
        let ic_de_chan = {
            let chan: Vec<_> = p_ic.all_de().iter().map(|h| h.value as f64).collect();
            if !chan.is_empty() {
                Some(chan)
            } else {
                None
            }
        };
        let ic_e_chan = {
            let chan: Vec<_> = p_ic.all_e().iter().map(|h| h.value as f64).collect();
            if !chan.is_empty() {
                Some(chan)
            } else {
                None
            }
        };

        let output_data = Data::new()
            .run_name(a.run_name.clone())
            .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
            .ic_x_energy(ic_e_x)
            .ic_y_energy(ic_e_y)
            .ic_de_energy(ic_e_de)
            .ic_e_energy(ic_e_e)
            .ic_tot_energy(ic_e_tot)
            .ic_pos_x(ic_pos_x)
            .ic_pos_y(ic_pos_y)
            .ic_theta(ic_theta)
            .ic_phi(ic_phi)
            .ic_x_chan(ic_x_chan)
            .ic_y_chan(ic_y_chan)
            .ic_de_chan(ic_de_chan)
            .ic_e_chan(ic_e_chan);
        output.push_data(output_data);
    }

    Ok(())
}
