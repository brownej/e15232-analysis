#[macro_use]
extern crate serde_derive;

use e15232_analysis::{analysis::Analysis, errors::*, packet::PacketizedEvent};
use std::f64::consts::FRAC_1_PI;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(get_method))?;
    analysis.register_start_func(Box::new(get_time_slice))?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process_ic))?;
    analysis.run()?;
    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Method {
    this_single: bool,
    other_single: bool,
    mid: MidMethod,
}

#[derive(Serialize, Deserialize, Debug)]
enum MidMethod {
    All,
    MidSingle,
    MidMulti,
    Beam(f64),
    NotBeam(f64),
}

fn get_method(a: &Analysis) -> Result<()> {
    let method: Method = serde_json::from_value(
        a.get_config()
            .other
            .get("method")
            .ok_or("method not supplied in config")?
            .clone(),
    )?;
    a.other_data
        .borrow_mut()
        .insert("method".to_string(), Box::new(method));
    Ok(())
}

// time_slice is optional
fn get_time_slice(a: &Analysis) -> Result<()> {
    if let Some(time_slice) = a.get_config().other.get("time_slice") {
        let time_slice: (f64, f64) = serde_json::from_value(time_slice.clone())?;
        a.other_data
            .borrow_mut()
            .insert("time_slice".to_string(), Box::new(time_slice));
    }
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process_ic(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let time_slice: Option<(f64, f64)> = analysis
        .other_data
        .borrow()
        .get("time_slice")
        .map(|x| {
            x.downcast_ref::<(f64, f64)>()
                .ok_or("time_slice is not an Option<(f64, f64)>")
        })
        .transpose()?
        .cloned();

    for p_ic in &pack.ic {
        if let Some(time_slice) = time_slice {
            if p_ic.time() < time_slice.0 || p_ic.time() > time_slice.1 {
                continue;
            }
        }

        for h_ic_e in p_ic.all_e() {
            let v_e = f64::from(h_ic_e.value);
            let other_data = analysis.other_data.borrow();
            let method = other_data
                .get("method")
                .ok_or("method not in other_data")?
                .downcast_ref::<Method>()
                .ok_or("method is not a Method")?;

            if !method.this_single || p_ic.all_x().len() == 1 {
                for h_ic_x in p_ic.all_x() {
                    let accept = match method.mid {
                        MidMethod::All => true,
                        MidMethod::MidSingle => {
                            p_ic.all_y().iter().any(|h_ic_y| h_ic_y.detid.1 == 15)
                        }
                        MidMethod::MidMulti => p_ic
                            .all_y()
                            .iter()
                            .any(|h_ic_y| h_ic_y.detid.1 >= 13 && h_ic_y.detid.1 < 17),
                        MidMethod::Beam(size) => p_ic.all_y().iter().any(|h_ic_y| {
                            ic_angles(f64::from(h_ic_x.detid.1), f64::from(h_ic_y.detid.1)).0
                                <= size
                        }),
                        MidMethod::NotBeam(size) => p_ic.all_y().iter().any(|h_ic_y| {
                            ic_angles(f64::from(h_ic_x.detid.1), f64::from(h_ic_y.detid.1)).0 > size
                        }),
                    };
                    if accept && (method.other_single || p_ic.all_y().len() == 1) {
                        let chan_x = h_ic_x.detid.1;
                        let v_x = f64::from(h_ic_x.value);
                        analysis.fill_2d(&format!("ic_val_x{:02}_e__xydee", chan_x), v_e, v_x)?;
                        analysis.fill_2d("ic_x_xch", f64::from(chan_x), v_x)?;
                        analysis.fill_1d("ic_xch", f64::from(chan_x))?;
                    }
                }
            }
            if !method.this_single || p_ic.all_y().len() == 1 {
                for h_ic_y in p_ic.all_y() {
                    let accept = match method.mid {
                        MidMethod::All => true,
                        MidMethod::MidSingle => {
                            p_ic.all_x().iter().any(|h_ic_x| h_ic_x.detid.1 == 15)
                        }
                        MidMethod::MidMulti => p_ic
                            .all_x()
                            .iter()
                            .any(|h_ic_x| h_ic_x.detid.1 >= 13 && h_ic_x.detid.1 < 17),
                        MidMethod::Beam(size) => p_ic.all_x().iter().any(|h_ic_x| {
                            ic_angles(f64::from(h_ic_x.detid.1), f64::from(h_ic_y.detid.1)).0
                                <= size
                        }),
                        MidMethod::NotBeam(size) => p_ic.all_x().iter().any(|h_ic_x| {
                            ic_angles(f64::from(h_ic_x.detid.1), f64::from(h_ic_y.detid.1)).0 > size
                        }),
                    };
                    if accept && (method.other_single || p_ic.all_x().len() == 1) {
                        let chan_y = h_ic_y.detid.1;
                        let v_y = f64::from(h_ic_y.value);
                        analysis.fill_2d(&format!("ic_val_y{:02}_e__xydee", chan_y), v_e, v_y)?;
                        analysis.fill_2d("ic_y_ych", f64::from(chan_y), v_y)?;
                        analysis.fill_1d("ic_ych", f64::from(chan_y))?;
                    }
                }
            }
            for h_ic_de in p_ic.all_de() {
                let v_de = f64::from(h_ic_de.value);
                analysis.fill_2d("ic_val_de_e__xydee", v_e, v_de)?;
            }
        }
    }

    Ok(())
}

pub fn ic_angles(ic_pos_x: f64, ic_pos_y: f64) -> (f64, f64) {
    let ic_dist_z_x = 558.8 + 20.0 + 18.3;
    let ic_dist_z_y = ic_dist_z_x + 36.6;
    let ic_pos_center_x = 16.597;
    let ic_pos_center_y = 15.000;

    let ic_dist_x = (ic_pos_x - ic_pos_center_x) * 3.0;
    let ic_dist_y = (ic_pos_y - ic_pos_center_y) * 3.0;

    let ic_theta = 180.
        * FRAC_1_PI
        * f64::atan(f64::sqrt(
            (ic_dist_x * ic_dist_x) / (ic_dist_z_x * ic_dist_z_x)
                + (ic_dist_y * ic_dist_y) / (ic_dist_z_y * ic_dist_z_y),
        ));
    let ic_phi = 180. * FRAC_1_PI * f64::atan2(-ic_dist_y, ic_dist_x);

    (ic_theta, ic_phi)
}
