use e15232_analysis::{analysis::Analysis, errors::*, packet::PacketizedEvent};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_ic))?;
    analysis.run()?;
    Ok(())
}

fn process_ic(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_ic in &pack.ic {
        let e_de = p_ic.comp_de().map(|x| x.energy);
        let e_e = p_ic.comp_e().map(|x| x.energy);

        if let (Some(e_de), Some(e_e)) = (e_de, e_e) {
            analysis.fill_2d("ic_e_de", e_e, e_de)?;
        }
    }

    Ok(())
}
