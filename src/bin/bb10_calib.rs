use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    let c_si_de60_e60_p = a.get_cut_2d("si_de60_e60_p")?;

    for p_si in &pack.si {
        for h_de in p_si.all_de() {
            if let Some(ch_e) = p_si.comp_e() {
                a.push_4d(
                    "dE_E",
                    f64::from(p_si.detno()),
                    f64::from(h_de.detid.1),
                    ch_e.energy,
                    f64::from(h_de.value),
                )?;

                for p_ic in &pack.ic {
                    if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                        a.push_4d(
                            "dE_E__tac",
                            f64::from(p_si.detno()),
                            f64::from(h_de.detid.1),
                            ch_e.energy,
                            f64::from(h_de.value),
                        )?;

                        if c_si_de60_e60_p.contains(ch_e.energy, f64::from(h_de.value)) {
                            a.push_4d(
                                "dE_E__tac_p",
                                f64::from(p_si.detno()),
                                f64::from(h_de.detid.1),
                                ch_e.energy,
                                f64::from(h_de.value),
                            )?;
                        }
                    }
                }
            }
        }
    }

    Ok(())
}
