use e15232_analysis::{
    analysis::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{
        is_si_alpha, is_si_proton,
        output::{print_formatted, Data, Output},
        particles::Ejectile,
        Hit,
    },
};
use std::cell::RefCell;

thread_local! {
    static EVENT_ID: RefCell<u64> = RefCell::new(0);
}

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(add_output))?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.register_acceptance_func(Box::new(accept))?;
    analysis.register_end_func(Box::new(print_output))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().thickness.is_none() {
        Err("thickness file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().run_info.is_none() {
        Err("run_info file not supplied in config")?
    }

    Ok(())
}

fn add_output(a: &Analysis) -> Result<()> {
    a.other_data
        .borrow_mut()
        .insert("output".to_string(), Box::new(Output::new()));
    Ok(())
}

fn print_output(a: &Analysis) -> Result<()> {
    print_formatted(
        a.other_data
            .borrow()
            .get("output")
            .ok_or("output not in other_data")?
            .downcast_ref::<Output>()
            .ok_or("output is not an Output")?,
    )?;
    println!();
    Ok(())
}

fn accept(_: &Analysis, h: &Hit) -> bool {
    match h.detid.0 {
        1..=42 => !(h.value == 0 || h.value > 16000),
        43 => false,
        _ => true,
    }
}

#[allow(non_snake_case)]
fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    let mut output = a.other_data.borrow_mut();
    let output = output
        .get_mut("output")
        .ok_or("output not in other_data")?
        .downcast_mut::<Output>()
        .ok_or("output is not an Output")?;

    let mut has_particle = false;
    let mut tac_gated = false;
    let event_id = EVENT_ID.with(|x| *x.borrow());

    for p_ic in &pack.ic {
        for p_si in &pack.si {
            tac_gated = c_tac_icst_si.contains(p_ic.time() - p_si.time());
        }
    }

    for p_si in &pack.si {
        let particle = if is_si_proton(&a, &p_si)? {
            Some(Ejectile::Proton)
        } else if is_si_alpha(&a, &p_si)? {
            Some(Ejectile::Alpha)
        //} else if is_si_single(&p_si)? {
        //    Some(Ejectile::Unknown)
        } else {
            None
        };

        if tac_gated {
            if let Some(part) = particle {
                has_particle = true;

                let si_theta = if let Some(ch) = p_si.comp_e() {
                    Some(a.get_angle_info(ch.detnum, ch.detch)?.theta_avg)
                } else {
                    None
                };
                let si_phi_ch = match (p_si.comp_de(), p_si.comp_be(), p_si.comp_e()) {
                    (Some(ch), _, _) => Some(ch),
                    (_, Some(ch), _) => Some(ch),
                    (_, _, Some(ch)) => Some(ch),
                    (_, _, _) => None,
                };
                let si_phi = if let Some(ch) = si_phi_ch {
                    Some(a.get_angle_info(ch.detnum, ch.detch)?.phi_avg)
                } else {
                    None
                };
                let detno = p_si.detno();

                for de in p_si.all_de() {
                    let output_data = Data::new()
                        .run_name(a.run_name.clone())
                        .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                        .event_id(event_id)
                        .ejectile_type(part)
                        .si_de_detid(de.detid.1)
                        .si_de_energy(de.energy.val)
                        .si_detno(detno)
                        .si_theta(si_theta)
                        .si_phi(si_phi);
                    output.push_data(output_data);
                }
                for e in p_si.all_e() {
                    let output_data = Data::new()
                        .run_name(a.run_name.clone())
                        .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                        .event_id(event_id)
                        .ejectile_type(part)
                        .si_e_detid(e.detid.1)
                        .si_e_energy(e.energy.val)
                        .si_detno(detno)
                        .si_theta(si_theta)
                        .si_phi(si_phi);
                    output.push_data(output_data);
                }
            }
        }
    }

    for p_ic in &pack.ic {
        if has_particle && tac_gated {
            let ic_dist_z_x = 558.8 + 20. + 18.3;
            let ic_dist_z_y = ic_dist_z_x + 36.6;
            let ic_pos_center_x = 16.597;
            let ic_pos_center_y = 15.000;

            let ic_pos_x = p_ic.comp_x().map(|ch| ch.detch);
            let ic_pos_y = p_ic.comp_y().map(|ch| ch.detch);
            let ic_dist_x = ic_pos_x.map(|p| (p - ic_pos_center_x) * 3.0);
            let ic_dist_y = ic_pos_y.map(|p| (p - ic_pos_center_y) * 3.0);
            let ic_theta = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                Some(
                    f64::atan(f64::sqrt(
                        (d_x * d_x) / (ic_dist_z_x * ic_dist_z_x)
                            + (d_y * d_y) / (ic_dist_z_y * ic_dist_z_y),
                    ))
                    .to_degrees(),
                )
            } else {
                None
            };
            let ic_phi = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                Some(f64::atan2(-d_y, d_x).to_degrees())
            } else {
                None
            };

            for x in p_ic.all_x() {
                let output_data = Data::new()
                    .run_name(a.run_name.clone())
                    .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                    .event_id(event_id)
                    .ic_x_detid(x.detid.1)
                    .ic_x_energy(x.energy.val)
                    .ic_pos_x(ic_pos_x)
                    .ic_pos_y(ic_pos_y)
                    .ic_theta(ic_theta)
                    .ic_phi(ic_phi);
                output.push_data(output_data);
            }
            for y in p_ic.all_y() {
                let output_data = Data::new()
                    .run_name(a.run_name.clone())
                    .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                    .event_id(event_id)
                    .ic_y_detid(y.detid.1)
                    .ic_y_energy(y.energy.val)
                    .ic_pos_x(ic_pos_x)
                    .ic_pos_y(ic_pos_y)
                    .ic_theta(ic_theta)
                    .ic_phi(ic_phi);
                output.push_data(output_data);
            }
            for de in p_ic.all_de() {
                let output_data = Data::new()
                    .run_name(a.run_name.clone())
                    .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                    .event_id(event_id)
                    .ic_de_detid(de.detid.1)
                    .ic_de_energy(de.energy.val)
                    .ic_pos_x(ic_pos_x)
                    .ic_pos_y(ic_pos_y)
                    .ic_theta(ic_theta)
                    .ic_phi(ic_phi);
                output.push_data(output_data);
            }
            for e in p_ic.all_e() {
                let output_data = Data::new()
                    .run_name(a.run_name.clone())
                    .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                    .event_id(event_id)
                    .ic_e_detid(e.detid.1)
                    .ic_e_energy(e.energy.val)
                    .ic_pos_x(ic_pos_x)
                    .ic_pos_y(ic_pos_y)
                    .ic_theta(ic_theta)
                    .ic_phi(ic_phi);
                output.push_data(output_data);
            }
        }
    }

    EVENT_ID.with(|x| *x.borrow_mut() += 1);

    Ok(())
}
