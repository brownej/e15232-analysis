use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for h_ic in &pack.ic {
        for ic_x in h_ic.comp_x() {
            for ic_y in h_ic.comp_y() {
                a.fill_2d("ic_pixels", ic_x.detch, ic_y.detch)?;
            }
        }
    }

    Ok(())
}
