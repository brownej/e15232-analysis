use datakiste::{io::Datakiste, DaqId};
use getopts::Options;
use std::{collections::HashMap, env, fs::File, io::BufReader};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Parse the command line arguments
    let args: Vec<String> = env::args().collect();
    let opts = Options::new();
    let matches = opts.parse(&args[1..]).unwrap();
    if matches.free.len() != 1 {
        panic!("Wrong number of args.");
    }

    // Create filenames
    let f_in_name = &matches.free[0];

    // Open files
    let f_in = BufReader::new(File::open(f_in_name)?);

    // Read in run from input file
    let dk: Datakiste = bincode::deserialize_from(f_in)?;

    for (n, i) in dk {
        if let Some(run) = i.into_run() {
            let mut sums = HashMap::<DaqId, u32>::new();
            for e in &run.events {
                for h1 in &e.hits {
                    *sums.entry(h1.daqid).or_insert(0) += 1;
                }
            }

            println!("{}", n);
            for (id, s) in sums {
                println!("{:?}: {}", id, s);
            }
        }
    }

    Ok(())
}
