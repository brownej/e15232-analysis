use self::InterpolationResult::*;
use e15232_analysis::errors::*;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

#[derive(PartialEq, Debug)]
pub enum InterpolationResult {
    InterpolatedValue(f64),
    ExtrapolatedValue(f64, f64),
    NoValue,
}

impl InterpolationResult {
    pub fn is_interp(&self) -> bool {
        match *self {
            InterpolatedValue(_) => true,
            _ => false,
        }
    }

    pub fn is_extrap(&self) -> bool {
        match *self {
            ExtrapolatedValue(_, _) => true,
            _ => false,
        }
    }

    pub fn is_value(&self) -> bool {
        match *self {
            InterpolatedValue(_) | ExtrapolatedValue(_, _) => true,
            _ => false,
        }
    }

    pub fn to_interp(&self) -> Option<f64> {
        match *self {
            InterpolatedValue(v) => Some(v),
            _ => None,
        }
    }

    pub fn to_extrap(&self) -> Option<f64> {
        match *self {
            ExtrapolatedValue(v, _) => Some(v),
            _ => None,
        }
    }

    pub fn to_value(&self) -> Option<f64> {
        match *self {
            InterpolatedValue(v) | ExtrapolatedValue(v, _) => Some(v),
            _ => None,
        }
    }

    pub fn to_clamp(&self) -> Option<f64> {
        match *self {
            InterpolatedValue(v) | ExtrapolatedValue(_, v) => Some(v),
            _ => None,
        }
    }
}

fn interpolate(x: f64, xs: &[f64], ys: &[f64]) -> InterpolationResult {
    use self::InterpolationResult::*;

    if xs.is_empty() {
        return NoValue;
    }

    match xs.binary_search_by(|v| v.partial_cmp(&x).expect("error in binary search")) {
        Ok(i) => {
            let y = ys.get(i).expect("error getting y0 in interpolator");

            InterpolatedValue(*y)
        }
        Err(i) => {
            if i == 0 {
                let x0 = xs.get(i).expect("error getting x0 in interpolator");
                let y0 = ys.get(i).expect("error getting y0 in interpolator");
                if let (Some(x1), Some(y1)) = (xs.get(i + 1), ys.get(i + 1)) {
                    ExtrapolatedValue((y1 - y0) / (x1 - x0) * (x - x0) + y0, *y0)
                } else {
                    ExtrapolatedValue(*y0, *y0)
                }
            } else if i == xs.len() {
                let x1 = xs.get(i - 1).expect("error getting x1 in interpolator");
                let y1 = ys.get(i - 1).expect("error getting y1 in interpolator");
                if let (Some(x0), Some(y0)) = (xs.get(i - 2), ys.get(i - 2)) {
                    ExtrapolatedValue((y1 - y0) / (x1 - x0) * (x - x1) + y1, *y1)
                } else {
                    ExtrapolatedValue(*y1, *y1)
                }
            } else {
                let x0 = xs.get(i - 1).expect("error getting x0 in interpolator");
                let y0 = ys.get(i - 1).expect("error getting y0 in interpolator");
                let x1 = xs.get(i).expect("error getting x1 in interpolator");
                let y1 = ys.get(i).expect("error getting y1 in interpolator");

                InterpolatedValue((y1 - y0) / (x1 - x0) * (x - x0) + y0)
            }
        }
    }
}

struct Efficiency {
    xs: Vec<f64>,
    ys: Vec<f64>,
}

impl Efficiency {
    fn call(&self, x: f64) -> InterpolationResult {
        interpolate(x, &self.xs, &self.ys)
    }
}

struct RunInfo {
    rhoa: f64,
}

#[allow(dead_code)]
#[derive(Debug)]
struct Event {
    run: String,
    p_ic: f64,
    part: String,
    ic_x: Option<f64>,
    ic_y: Option<f64>,
    ic_de: Option<f64>,
    ic_e: Option<f64>,
    theta_r: Option<f64>,
    phi_r: Option<f64>,
    si_de: Option<f64>,
    si_e: Option<f64>,
    theta_e: f64,
    phi_e: f64,
    detno: u32,
    q_val: f64,
    theta_cm: f64,
    probs: (f64, f64, f64),
}

fn read_events<T: AsRef<Path>>(p: T) -> Result<Vec<Event>> {
    let f = File::open(p)?;
    let f = BufReader::new(f);

    let mut events = Vec::new();

    for l in f.lines() {
        let l = l?;
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else {
            let run = x[0].to_string();
            let p_ic = x[1].parse::<f64>()?;
            let part = x[2].to_string();
            let ic_x = x[3].parse::<f64>().ok();
            let ic_y = x[4].parse::<f64>().ok();
            let ic_de = x[5].parse::<f64>().ok();
            let ic_e = x[6].parse::<f64>().ok();
            let theta_r = x[7].parse::<f64>().ok();
            let phi_r = x[8].parse::<f64>().ok();
            let si_de = x[9].parse::<f64>().ok();
            let si_e = x[10].parse::<f64>().ok();
            let theta_e = x[11].parse::<f64>()?;
            let phi_e = x[12].parse::<f64>()?;
            let detno = x[13].parse::<u32>()?;
            let q_val = x[14].parse::<f64>()?;
            let theta_cm = x[15].parse::<f64>()?;
            let probs = (
                x[16].parse::<f64>()?,
                x[17].parse::<f64>()?,
                x[18].parse::<f64>()?,
            );

            events.push(Event {
                run,
                p_ic,
                part,
                ic_x,
                ic_y,
                ic_de,
                ic_e,
                theta_r,
                phi_r,
                si_de,
                si_e,
                theta_e,
                phi_e,
                detno,
                q_val,
                theta_cm,
                probs,
            });
        }
    }

    Ok(events)
}

fn read_run_info<T: AsRef<Path>>(p: T) -> Result<HashMap<String, RunInfo>> {
    let f = File::open(p)?;
    let f = BufReader::new(f);

    let mut run_info = HashMap::new();

    for l in f.lines() {
        let l = l?;
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else {
            let run = x[0].to_string();
            let rhoa = x[8].parse::<f64>()?;

            let old = run_info.insert(run.to_string(), RunInfo { rhoa });
            if old.is_some() {
                eprintln!("WARNING: There is already run info for run `{}`.", run);
            }
        }
    }

    Ok(run_info)
}

fn read_effs<T: AsRef<Path>>(
    p: T,
    q_vals: &HashMap<String, Vec<f64>>,
) -> Result<HashMap<String, Efficiency>> {
    let f = File::open(p)?;
    let f = BufReader::new(f);

    let mut effs = HashMap::new();

    for l in f.lines() {
        let l = l?;
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else {
            let beam_in = x[0];
            let level = x[1].parse::<usize>()?;
            let section = x[2];
            let eff = x[3].parse::<f64>()?;
            let q_val = q_vals
                .get(beam_in)
                .ok_or("no q-value list for beam")?
                .get(level)
                .ok_or("no q-value for level")?;
            let id = format!("{} {}", beam_in, section);

            let entry = effs.entry(id).or_insert((vec![], vec![]));
            entry.0.push(*q_val);
            entry.1.push(eff);
        }
    }

    let mut ret = HashMap::new();
    for (k, (v0, v1)) in effs {
        ret.insert(
            k,
            Efficiency {
                xs: v0.to_vec(),
                ys: v1.to_vec(),
            },
        );
    }

    Ok(ret)
}

fn read_q_vals<T: AsRef<Path>>(p: T) -> Result<HashMap<String, Vec<f64>>> {
    let f = File::open(p)?;
    let f = BufReader::new(f);

    let mut q_vals = HashMap::new();

    for l in f.lines() {
        let l = l?;
        let x: Vec<_> = l.split_whitespace().collect();
        if x.is_empty() || x[0].starts_with('#') {
            continue;
        } else {
            let beam_in = x[0].to_string();
            let q_val = x[2].parse::<f64>()?;

            q_vals.entry(beam_in).or_insert_with(Vec::new).push(q_val);
        }
    }

    Ok(q_vals)
}

fn accept_event(e: &Event) -> bool {
    if e.part == "a" {
        return false;
    }
    if get_section(e) == "SORR_DNS" {
        return false;
    }
    if let (Some(theta_r), Some(si_e)) = (e.theta_r, e.si_e) {
        (theta_r > 0.6) && (si_e > 500.0)
    } else {
        false
    }
}

fn get_section(e: &Event) -> &'static str {
    let d = e.detno;
    if d >= 15 && d <= 19 {
        "SIDAR"
    } else if d == 8 || d == 10 || d == 11 || d == 14 {
        "SORR_DNT"
    } else if d == 9 || d == 13 {
        "SORR_DNS"
    } else if d >= 1 && d <= 7 {
        "SORR_UP"
    } else {
        panic!()
    }
}

fn main_catch_err() -> Result<()> {
    let events = read_events("recoils")?;
    let run_info = read_run_info("run_info")?;
    let q_vals = read_q_vals("q_vals")?;
    let effs = read_effs("efficiencies", &q_vals)?;

    // sigma = n/(i t rhoa eff)
    let mut total_s34 = 0.0;
    let mut total_cl34 = 0.0;
    let mut total_ar34 = 0.0;

    for e in events {
        //println!("{:?}", e);
        if accept_event(&e) {
            let rhoa = run_info.get(&e.run).ok_or("no rhoa in run info")?.rhoa;
            let probs = e.probs;
            let q_val = e.q_val;
            let section = get_section(&e);
            let eff = (
                effs.get(&format!("34S {}", section))
                    .ok_or_else(|| format!("no efficiency info for 34S {}", section))?
                    .call(q_val),
                effs.get(&format!("34Cl {}", section))
                    .ok_or_else(|| format!("no efficiency info for 34Cl {}", section))?
                    .call(q_val),
                effs.get(&format!("34Ar {}", section))
                    .ok_or_else(|| format!("no efficiency info for 34Ar {}", section))?
                    .call(q_val),
            );

            let _eff_s34 = eff.0.to_clamp().ok_or_else(|| "no eff")?;
            let _eff_cl34 = eff.1.to_clamp().ok_or_else(|| "no eff")?;
            let _eff_ar34 = eff.2.to_clamp().ok_or_else(|| "no eff")?;

            let eff_s34 = 0.113_834_982_014_535 + 0.058_722_619_726_964_8 + 0.037_449_416_438_299_5;
            let eff_cl34 =
                0.100_484_408_933_851 + 0.058_952_647_924_323_5 + 0.049_839_264_771_068_9;
            let eff_ar34 =
                0.099_168_077_074_819_9 + 0.059_012_205_148_217_8 + 0.051_469_334_035_862_2;

            if eff_s34 != 0.0 {
                total_s34 += probs.0 / (rhoa / (1e27 / 1e19)) / eff_s34;
            }
            if eff_cl34 != 0.0 {
                total_cl34 += probs.1 / (rhoa / (1e27 / 1e19)) / eff_cl34;
            }
            if eff_ar34 != 0.0 {
                total_ar34 += probs.2 / (rhoa / (1e27 / 1e19)) / eff_ar34;
            }

            //            println!("probs {:?}", probs);
            //            println!("rhoa {}", rhoa);
            //            println!("totals {} {} {}", total_s34, total_cl34, total_ar34);
            //            println!("");
        }
    }

    let s34_num = 8.229_35e+07;
    let cl34_num = 3.153_39e+08;
    let ar34_num = 5.860_14e+08;
    let num_sum = s34_num + cl34_num + ar34_num;

    let n_s34 = 1_833_324_839.0 * s34_num / num_sum;
    let n_cl34 = 1_833_324_839.0 * cl34_num / num_sum;
    let n_ar34 = 1_833_324_839.0 * ar34_num / num_sum;

    println!("34S: {} mb", total_s34 / n_s34);
    println!("34Cl: {} mb", total_cl34 / n_cl34);
    println!("34Ar: {} mb", total_ar34 / n_ar34);

    Ok(())
}

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}
