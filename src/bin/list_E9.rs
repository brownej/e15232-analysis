#![allow(non_snake_case)]
use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn process(_a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_si in &pack.si {
        for h in p_si.all_e() {
            if h.detid.0 == 8 {
                println!("{}\t{}\t{}", h.detid.0, h.detid.1, h.energy.val);
            }
        }
    }

    Ok(())
}
