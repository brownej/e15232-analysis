use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_ic))?;
    analysis.run()?;
    Ok(())
}

fn process_ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_ic in &pack.ic {
        for h in p_ic.all_x() {
            a.fill_1d("ic_xpos", f64::from(h.detid.1))?;
        }
        for h in p_ic.all_y() {
            a.fill_1d("ic_ypos", f64::from(h.detid.1))?;
        }
    }

    Ok(())
}
