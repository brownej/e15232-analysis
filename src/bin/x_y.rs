use e15232_analysis::{
    analysis::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{is_si_alpha, is_si_proton},
};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.run()?;
    Ok(())
}

#[allow(non_snake_case)]
fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    for p_ic in &pack.ic {
        if let (Some(ch_x), Some(ch_y)) = (p_ic.comp_x(), p_ic.comp_y()) {
            a.fill_2d("ic_x_y", ch_y.energy, ch_x.energy)?;
            if (ch_y.detch - 16.0).abs() <= std::f64::EPSILON
                && (ch_x.detch - 16.0).abs() <= std::f64::EPSILON
            {
                a.fill_2d("ic_x16_y16", ch_y.energy, ch_x.energy)?;
            }
            for p_si in &pack.si {
                if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                    a.fill_2d("ic_x_y__tac", ch_y.energy, ch_x.energy)?;
                    if (ch_y.detch - 16.0).abs() <= std::f64::EPSILON
                        && (ch_x.detch - 16.0).abs() <= std::f64::EPSILON
                    {
                        a.fill_2d("ic_x16_y16__tac", ch_y.energy, ch_x.energy)?;
                    }
                    if is_si_proton(&a, &p_si)? {
                        a.push_2d("ic_x_y__p_tac", ch_y.energy, ch_x.energy)?;
                    }
                    if is_si_alpha(&a, &p_si)? {
                        a.push_2d("ic_x_y__a_tac", ch_y.energy, ch_x.energy)?;
                    }
                }
            }
        }
    }

    Ok(())
}
