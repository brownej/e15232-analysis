use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    for h_si in &pack.si {
        let mut tac = false;
        for h_ic in &pack.ic {
            if c_tac_icst_si.contains(h_ic.time() - h_si.time()) {
                tac = true;
            }
        }

        for si_de in h_si.comp_de() {
            for si_e in h_si.comp_e() {
                let (de60, e60) = a.si_thick_corr(&si_de, &si_e)?;
                a.fill_2d("si_e60_de60", e60, de60)?;
                if tac {
                    a.fill_2d("si_e60_de60__tac", e60, de60)?;
                }
            }
        }
    }

    Ok(())
}
