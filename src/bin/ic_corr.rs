#![allow(non_snake_case)]
use e15232_analysis::{
    analysis::{Analysis, Reaction},
    errors::*,
    packet::PacketizedEvent,
    util::{
        is_si_alpha, is_si_proton, is_si_single,
        output::{print_formatted, Data, Output},
        particles::{Ejectile, Recoil},
        Hit,
    },
};
use std::f64::consts::FRAC_1_PI;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(add_output))?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.register_acceptance_func(Box::new(accept))?;
    analysis.register_end_func(Box::new(print_output))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().thickness.is_none() {
        Err("thickness file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().run_info.is_none() {
        Err("run_info file not supplied in config")?
    }

    Ok(())
}

fn add_output(a: &Analysis) -> Result<()> {
    a.other_data
        .borrow_mut()
        .insert("output".to_string(), Box::new(Output::new()));
    Ok(())
}

fn print_output(a: &Analysis) -> Result<()> {
    print_formatted(
        a.other_data
            .borrow()
            .get("output")
            .ok_or("output not in other_data")?
            .downcast_ref::<Output>()
            .ok_or("output is not an Output")?,
    )?;
    println!();
    Ok(())
}

fn accept(_: &Analysis, h: &Hit) -> bool {
    match h.detid.0 {
        1..=42 => !(h.value == 0 || h.value > 16000),
        43 => false,
        _ => true,
    }
}

fn probs_si(q_vals: &[(Reaction, f64, f64)], e: f64) -> (f64, f64, f64) {
    /*
    let s34_num = 8.22935e+07;
    let cl34_num = 3.15339e+08;
    let ar34_num = 5.86014e+08;
    let num_sum = s34_num + cl34_num + ar34_num;

    let s34_frac = s34_num / num_sum;
    let cl34_frac = cl34_num / num_sum;
    let ar34_frac = ar34_num / num_sum;
    */

    let mut s34_prob_sum = 0.0;
    let mut cl34_prob_sum = 0.0;
    let mut ar34_prob_sum = 0.0;

    for &(ref r, ref q, ref _sigma) in q_vals {
        let prob_sum: &mut f64;
        match *r {
            Reaction::S34 => {
                prob_sum = &mut s34_prob_sum;
            }
            Reaction::Cl34 => {
                prob_sum = &mut cl34_prob_sum;
            }
            Reaction::Ar34 => {
                prob_sum = &mut ar34_prob_sum;
            }
        };
        /*
        let _frac: &f64;
        match *r {
            Reaction::S34 => { prob_sum = &mut s34_prob_sum; _frac = &s34_frac; }
            Reaction::Cl34 => { prob_sum = &mut cl34_prob_sum; _frac = &cl34_frac; }
            Reaction::Ar34 => { prob_sum = &mut ar34_prob_sum; _frac = &ar34_frac; }
        };
        */

        //let prob = sigma * frac * f64::exp(-(q - e).powi(2) / (15.0f64).powi(2) / 2.0);
        let prob = f64::exp(-(q - e).powi(2) / (15.0f64).powi(2) / 2.0);
        *prob_sum += prob;
    }

    let sum = s34_prob_sum + cl34_prob_sum + ar34_prob_sum;
    if sum == 0.0 {
        (0.0, 0.0, 0.0)
    } else {
        (s34_prob_sum / sum, cl34_prob_sum / sum, ar34_prob_sum / sum)
    }
}

fn probs_ic(e_x: f64) -> (f64, f64, f64) {
    let resolution = 236.138f64;
    let e_x_s34 = 6415.88;
    let e_x_cl34 = 5867.48;
    let e_x_ar34 = 5331.32;

    /*
    let s34_num = 8.22935e+07;
    let cl34_num = 3.15339e+08;
    let ar34_num = 5.86014e+08;
    let num_sum = s34_num + cl34_num + ar34_num;

    let s34_frac = s34_num / num_sum;
    let cl34_frac = cl34_num / num_sum;
    let ar34_frac = ar34_num / num_sum;
    */

    let mut s34_prob_sum = 0.0;
    let mut cl34_prob_sum = 0.0;
    let mut ar34_prob_sum = 0.0;

    let prob = f64::exp(-(e_x - e_x_s34).powi(2) / (resolution).powi(2) / 2.0);
    s34_prob_sum += prob;

    let prob = f64::exp(-(e_x - e_x_cl34).powi(2) / (resolution).powi(2) / 2.0);
    cl34_prob_sum += prob;

    let prob = f64::exp(-(e_x - e_x_ar34).powi(2) / (resolution).powi(2) / 2.0);
    ar34_prob_sum += prob;

    let sum = s34_prob_sum + cl34_prob_sum + ar34_prob_sum;
    if sum == 0.0 {
        (0.0, 0.0, 0.0)
    } else {
        (s34_prob_sum / sum, cl34_prob_sum / sum, ar34_prob_sum / sum)
    }
}

fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    let mut output = a.other_data.borrow_mut();
    let output = output
        .get_mut("output")
        .ok_or("output not in other_data")?
        .downcast_mut::<Output>()
        .ok_or("output is not an Output")?;

    for p_ic in &pack.ic {
        for p_si in &pack.si {
            if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                let particle = if is_si_proton(&a, &p_si)? {
                    Some(Ejectile::Proton)
                } else if is_si_alpha(&a, &p_si)? {
                    Some(Ejectile::Alpha)
                } else if is_si_single(&p_si)? {
                    Some(Ejectile::Unknown)
                } else {
                    None
                };

                if let Some(part) = particle {
                    let ic_dist_z_x = 558.8 + 20. + 18.3;
                    let ic_dist_z_y = ic_dist_z_x + 36.6;
                    let ic_pos_center_x = 16.597;
                    let ic_pos_center_y = 15.000;

                    let ic_e_x = p_ic.comp_x().map(|ch| ch.energy);
                    let ic_e_y = p_ic.comp_y().map(|ch| ch.energy);
                    let ic_e_de = p_ic.comp_de().map(|ch| ch.energy);
                    let ic_e_e = p_ic.comp_e().map(|ch| ch.energy);
                    let ic_pos_x = p_ic.comp_x().map(|ch| ch.detch);
                    let ic_pos_y = p_ic.comp_y().map(|ch| ch.detch);
                    let ic_dist_x = ic_pos_x.map(|p| (p - ic_pos_center_x) * 3.0);
                    let ic_dist_y = ic_pos_y.map(|p| (p - ic_pos_center_y) * 3.0);
                    let ic_theta = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                        Some(
                            180. * FRAC_1_PI
                                * f64::atan(f64::sqrt(
                                    (d_x * d_x) / (ic_dist_z_x * ic_dist_z_x)
                                        + (d_y * d_y) / (ic_dist_z_y * ic_dist_z_y),
                                )),
                        )
                    } else {
                        None
                    };
                    let ic_phi = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                        Some(180. * FRAC_1_PI * f64::atan2(-d_y, d_x))
                    } else {
                        None
                    };

                    let si_e_de = p_si.comp_de().map(|ch| ch.energy);
                    let si_e_e = p_si.comp_e().map(|ch| ch.energy);
                    let si_e_tot = match (si_e_de, si_e_e) {
                        (Some(de), Some(e)) => Some(e + de),
                        (None, Some(e)) => Some(e),
                        (Some(de), None) => Some(de),
                        (None, None) => None,
                    };

                    let si_theta = if let Some(ch) = p_si.comp_e() {
                        Some(a.get_angle_info(ch.detnum, ch.detch)?.theta_avg)
                    } else {
                        None
                    };
                    let si_phi_ch = match (p_si.comp_de(), p_si.comp_be(), p_si.comp_e()) {
                        (Some(ch), _, _) => Some(ch),
                        (_, Some(ch), _) => Some(ch),
                        (_, _, Some(ch)) => Some(ch),
                        (_, _, _) => None,
                    };
                    let si_phi = if let Some(ch) = si_phi_ch {
                        Some(a.get_angle_info(ch.detnum, ch.detch)?.phi_avg)
                    } else {
                        None
                    };
                    let q_val = if let Some(th) = si_theta {
                        Some(a.get_q_val_34ar(th, si_e_de.unwrap_or(0.0) + si_e_e.unwrap_or(0.0))?)
                    } else {
                        None
                    };
                    let th_cm = if let Some(th) = si_theta {
                        Some(a.get_th_cm_34ar(th, si_e_de.unwrap_or(0.0) + si_e_e.unwrap_or(0.0))?)
                    } else {
                        None
                    };
                    let q_vals = a.get_q_vals()?;
                    let probs_si = q_val.map(|q| probs_si(q_vals, q));
                    let probs_ic = ic_e_x.map(probs_ic);

                    let ic_e_x_corr_34s = if let (Some(ic_x), Some(si_th), Some(si_e)) =
                        (ic_e_x, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37cl_corr(Some(ic_x), None, None, None)?
                                .0
                                .ok_or("problem correcting ic_x for pressure")?
                                + a.get_ic_x_34s(90., 1415.27)?
                                - a.get_ic_x_34s(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_y_corr_34s = if let (Some(ic_y), Some(si_th), Some(si_e)) =
                        (ic_e_y, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37cl_corr(None, Some(ic_y), None, None)?
                                .1
                                .ok_or("problem correcting ic_y for pressure")?
                                + a.get_ic_y_34s(90., 1415.27)?
                                - a.get_ic_y_34s(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_de_corr_34s = if let (Some(ic_de), Some(si_th), Some(si_e)) =
                        (ic_e_de, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37cl_corr(None, None, Some(ic_de), None)?
                                .2
                                .ok_or("problem correcting ic_de for pressure")?
                                + a.get_ic_de_34s(90., 1415.27)?
                                - a.get_ic_de_34s(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_e_corr_34s = if let (Some(ic_e), Some(si_th), Some(si_e)) =
                        (ic_e_e, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37cl_corr(None, None, None, Some(ic_e))?
                                .3
                                .ok_or("problem correcting ic_e for pressure")?
                                + a.get_ic_e_34s(90., 1415.27)?
                                - a.get_ic_e_34s(si_th, si_e)?,
                        )
                    } else {
                        None
                    };

                    let ic_e_x_corr_34cl = if let (Some(ic_x), Some(si_th), Some(si_e)) =
                        (ic_e_x, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37ar_corr(Some(ic_x), None, None, None)?
                                .0
                                .ok_or("problem correcting ic_x for pressure")?
                                + a.get_ic_x_34cl(90., 1415.27)?
                                - a.get_ic_x_34cl(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_y_corr_34cl = if let (Some(ic_y), Some(si_th), Some(si_e)) =
                        (ic_e_y, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37ar_corr(None, Some(ic_y), None, None)?
                                .1
                                .ok_or("problem correcting ic_y for pressure")?
                                + a.get_ic_y_34cl(90., 1415.27)?
                                - a.get_ic_y_34cl(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_de_corr_34cl = if let (Some(ic_de), Some(si_th), Some(si_e)) =
                        (ic_e_de, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37ar_corr(None, None, Some(ic_de), None)?
                                .2
                                .ok_or("problem correcting ic_de for pressure")?
                                + a.get_ic_de_34cl(90., 1415.27)?
                                - a.get_ic_de_34cl(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_e_corr_34cl = if let (Some(ic_e), Some(si_th), Some(si_e)) =
                        (ic_e_e, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37ar_corr(None, None, None, Some(ic_e))?
                                .3
                                .ok_or("problem correcting ic_e for pressure")?
                                + a.get_ic_e_34cl(90., 1415.27)?
                                - a.get_ic_e_34cl(si_th, si_e)?,
                        )
                    } else {
                        None
                    };

                    let ic_e_x_corr_34ar = if let (Some(ic_x), Some(si_th), Some(si_e)) =
                        (ic_e_x, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37k_corr(Some(ic_x), None, None, None)?
                                .0
                                .ok_or("problem correcting ic_x for pressure")?
                                + a.get_ic_x_34ar(90., 1415.27)?
                                - a.get_ic_x_34ar(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_y_corr_34ar = if let (Some(ic_y), Some(si_th), Some(si_e)) =
                        (ic_e_y, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37k_corr(None, Some(ic_y), None, None)?
                                .1
                                .ok_or("problem correcting ic_y for pressure")?
                                + a.get_ic_y_34ar(90., 1415.27)?
                                - a.get_ic_y_34ar(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_de_corr_34ar = if let (Some(ic_de), Some(si_th), Some(si_e)) =
                        (ic_e_de, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37k_corr(None, None, Some(ic_de), None)?
                                .2
                                .ok_or("problem correcting ic_de for pressure")?
                                + a.get_ic_de_34ar(90., 1415.27)?
                                - a.get_ic_de_34ar(si_th, si_e)?,
                        )
                    } else {
                        None
                    };
                    let ic_e_e_corr_34ar = if let (Some(ic_e), Some(si_th), Some(si_e)) =
                        (ic_e_e, si_theta, si_e_tot)
                    {
                        Some(
                            a.ic_37k_corr(None, None, None, Some(ic_e))?
                                .3
                                .ok_or("problem correcting ic_e for pressure")?
                                + a.get_ic_e_34ar(90., 1415.27)?
                                - a.get_ic_e_34ar(si_th, si_e)?,
                        )
                    } else {
                        None
                    };

                    let output_data = Data::new()
                        .run_name(a.run_name.clone())
                        .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                        .ejectile_type(part)
                        .ic_x_energy(ic_e_x)
                        .ic_y_energy(ic_e_y)
                        .ic_de_energy(ic_e_de)
                        .ic_e_energy(ic_e_e)
                        .ic_theta(ic_theta)
                        .ic_phi(ic_phi)
                        .si_de_energy(si_e_de)
                        .si_e_energy(si_e_e)
                        .si_theta(si_theta)
                        .si_phi(si_phi)
                        .q_value(q_val)
                        .theta_cm(th_cm)
                        .prob_si_34s(probs_si.map(|a| a.0))
                        .prob_si_34cl(probs_si.map(|a| a.1))
                        .prob_si_34ar(probs_si.map(|a| a.2))
                        .prob_ic_34s(probs_ic.map(|a| a.0))
                        .prob_ic_34cl(probs_ic.map(|a| a.1))
                        .prob_ic_34ar(probs_ic.map(|a| a.2));
                    let data_34s = output_data
                        .clone()
                        .recoil_type(Recoil::S34)
                        .ic_x_energy(ic_e_x_corr_34s)
                        .ic_y_energy(ic_e_y_corr_34s)
                        .ic_de_energy(ic_e_de_corr_34s)
                        .ic_e_energy(ic_e_e_corr_34s);
                    let data_34cl = output_data
                        .clone()
                        .recoil_type(Recoil::Cl34)
                        .ic_x_energy(ic_e_x_corr_34cl)
                        .ic_y_energy(ic_e_y_corr_34cl)
                        .ic_de_energy(ic_e_de_corr_34cl)
                        .ic_e_energy(ic_e_e_corr_34cl);
                    let data_34ar = output_data
                        .clone()
                        .recoil_type(Recoil::Ar34)
                        .ic_x_energy(ic_e_x_corr_34ar)
                        .ic_y_energy(ic_e_y_corr_34ar)
                        .ic_de_energy(ic_e_de_corr_34ar)
                        .ic_e_energy(ic_e_e_corr_34ar);
                    output.push_data(output_data);
                    output.push_data(data_34s);
                    output.push_data(data_34cl);
                    output.push_data(data_34ar);
                }
            }
        }
    }

    Ok(())
}
