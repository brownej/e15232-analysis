use e15232_analysis::util::output::{print_formatted, Data, Output};

fn main() {
    let mut output = Output::new();
    output.info.comment = Some("this is a test".to_string());
    output.push_data(Data::new().run_name("run0".to_string()));
    output.push_data(Data::new().run_name("run1".to_string()).run_pressure(100.));
    output.push_data(Data::new().run_name("run2".to_string()).run_pressure(200.));

    print_formatted(&output).unwrap();
    println!();
}
