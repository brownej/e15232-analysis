use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p in &pack.other {
        for h in p.all_hits() {
            let name = format!(
                "val_{}_{}_{}_{}",
                h.daqid.0, h.daqid.1, h.daqid.2, h.daqid.3
            );
            if a.contains_hist(&name) {
                a.fill_1d(&name, f64::from(h.value))?;
            }
        }
    }
    for p in &pack.ic {
        for h in p
            .all_x()
            .iter()
            .chain(p.all_y())
            .chain(p.all_de())
            .chain(p.all_e())
        {
            let name = format!(
                "val_{}_{}_{}_{}",
                h.daqid.0, h.daqid.1, h.daqid.2, h.daqid.3
            );
            if a.contains_hist(&name) {
                a.fill_1d(&name, f64::from(h.value))?;
            }
        }
    }
    for p in &pack.si {
        for h in p.all_e().iter().chain(p.all_de()).chain(p.all_be()) {
            let name = format!(
                "val_{}_{}_{}_{}",
                h.daqid.0, h.daqid.1, h.daqid.2, h.daqid.3
            );
            if a.contains_hist(&name) {
                a.fill_1d(&name, f64::from(h.value))?;
            }
        }
    }

    Ok(())
}
