use datakiste::io::Datakiste;
use getopts::Options;
use std::{env, fs::File, io::BufReader};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Parse the command line arguments
    let args: Vec<String> = env::args().collect();
    let opts = Options::new();
    let matches = opts.parse(&args[1..])?;
    if matches.free.len() != 1 {
        panic!("Wrong number of args.");
    }

    //Create filenames
    let f_in_name = &matches.free[0];

    // Open files
    let f_in = BufReader::new(File::open(f_in_name)?);

    // Read in run from input file
    let dk: Datakiste = bincode::deserialize_from(f_in)?;
    for (n, i) in dk {
        if let Some(run) = i.into_run() {
            println!("Run {}", n);
            for e in run.into_events() {
                for h in e.hits {
                    println!(
                        "{:?} {:?} {} {:?} {:?} {}",
                        h.daqid, h.detid, h.rawval, h.value, h.energy, h.time
                    );
                }
                println!();
            }
        }
    }

    Ok(())
}
