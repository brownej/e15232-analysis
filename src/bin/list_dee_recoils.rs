#![allow(non_snake_case)]
use e15232_analysis::{
    analysis::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{
        is_si_alpha, is_si_proton,
        output::{print_formatted, Data, Output},
        particles::Ejectile,
        Hit,
    },
};
use std::f64::consts::FRAC_1_PI;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(add_output))?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.register_acceptance_func(Box::new(accept))?;
    analysis.register_end_func(Box::new(print_output))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().thickness.is_none() {
        Err("thickness file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().run_info.is_none() {
        Err("run_info file not supplied in config")?
    }

    Ok(())
}

fn add_output(a: &Analysis) -> Result<()> {
    a.other_data
        .borrow_mut()
        .insert("output".to_string(), Box::new(Output::new()));
    Ok(())
}

fn print_output(a: &Analysis) -> Result<()> {
    print_formatted(
        a.other_data
            .borrow()
            .get("output")
            .ok_or("output not in other_data")?
            .downcast_ref::<Output>()
            .ok_or("output is not an Output")?,
    )?;
    println!();
    Ok(())
}

fn accept(_: &Analysis, h: &Hit) -> bool {
    match h.detid.0 {
        1..=42 => !(h.value == 0 || h.value > 16000),
        43 => false,
        _ => true,
    }
}

fn process_si__ic(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    let mut output = a.other_data.borrow_mut();
    let output = output
        .get_mut("output")
        .ok_or("output not in other_data")?
        .downcast_mut::<Output>()
        .ok_or("output is not an Output")?;

    for p_ic in &pack.ic {
        for p_si in &pack.si {
            if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                let particle = if is_si_proton(&a, &p_si)? {
                    Some(Ejectile::Proton)
                } else if is_si_alpha(&a, &p_si)? {
                    Some(Ejectile::Alpha)
                //} else if is_si_single(&p_si)? {
                //    Some(Ejectile::Unknown)
                } else {
                    None
                };

                if let Some(part) = particle {
                    let ic_dist_z_x = 558.8 + 20. + 18.3;
                    let ic_dist_z_y = ic_dist_z_x + 36.6;
                    let ic_pos_center_x = 16.597;
                    let ic_pos_center_y = 15.000;

                    let ic_e_x = p_ic.comp_x().map(|ch| ch.energy);
                    let ic_e_y = p_ic.comp_y().map(|ch| ch.energy);
                    let ic_e_de = p_ic.comp_de().map(|ch| ch.energy);
                    let ic_e_e = p_ic.comp_e().map(|ch| ch.energy);
                    let ic_pos_x = p_ic.comp_x().map(|ch| ch.detch);
                    let ic_pos_y = p_ic.comp_y().map(|ch| ch.detch);
                    let ic_dist_x = ic_pos_x.map(|p| (p - ic_pos_center_x) * 3.0);
                    let ic_dist_y = ic_pos_y.map(|p| (p - ic_pos_center_y) * 3.0);
                    let ic_theta = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                        Some(
                            180. * FRAC_1_PI
                                * f64::atan(f64::sqrt(
                                    (d_x * d_x) / (ic_dist_z_x * ic_dist_z_x)
                                        + (d_y * d_y) / (ic_dist_z_y * ic_dist_z_y),
                                )),
                        )
                    } else {
                        None
                    };
                    let ic_phi = if let (Some(d_x), Some(d_y)) = (ic_dist_x, ic_dist_y) {
                        Some(180. * FRAC_1_PI * f64::atan2(-d_y, d_x))
                    } else {
                        None
                    };

                    let si_e_de = p_si.comp_de().map(|ch| ch.energy);
                    let si_e_e = p_si.comp_e().map(|ch| ch.energy);
                    let si_theta = if let Some(ch) = p_si.comp_e() {
                        Some(a.get_angle_info(ch.detnum, ch.detch)?.theta_avg)
                    } else {
                        None
                    };
                    let si_phi_ch = match (p_si.comp_de(), p_si.comp_be(), p_si.comp_e()) {
                        (Some(ch), _, _) => Some(ch),
                        (_, Some(ch), _) => Some(ch),
                        (_, _, Some(ch)) => Some(ch),
                        (_, _, _) => None,
                    };
                    let si_phi = if let Some(ch) = si_phi_ch {
                        Some(a.get_angle_info(ch.detnum, ch.detch)?.phi_avg)
                    } else {
                        None
                    };

                    let _si_sum = si_e_de.unwrap_or(0.0) + si_e_e.unwrap_or(0.0);
                    let _ic_sum = ic_e_x.unwrap_or(0.0)
                        + ic_e_y.unwrap_or(0.0)
                        + ic_e_de.unwrap_or(0.0)
                        + ic_e_e.unwrap_or(0.0);

                    let output_data = Data::new()
                        .run_name(a.run_name.clone())
                        .run_pressure(a.get_run_info(&a.run_name)?.cap_ic.map(|x| x.0))
                        .ejectile_type(part)
                        .ic_x_energy(ic_e_x)
                        .ic_y_energy(ic_e_y)
                        .ic_de_energy(ic_e_de)
                        .ic_e_energy(ic_e_e)
                        .ic_pos_x(ic_pos_x)
                        .ic_pos_y(ic_pos_y)
                        .ic_theta(ic_theta)
                        .ic_phi(ic_phi)
                        .si_de_energy(si_e_de)
                        .si_e_energy(si_e_e)
                        .si_theta(si_theta)
                        .si_phi(si_phi);
                    output.push_data(output_data);
                }
            }
        }
    }

    Ok(())
}
