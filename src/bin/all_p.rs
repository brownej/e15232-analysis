use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_hits))?;
    analysis.run()?;
    Ok(())
}

fn process_hits(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_si in &pack.si {
        let mut is_p = false;
        let c_si_de60_e60_p = analysis.get_cut_2d("si_de60_e60_p")?;

        if let (Some(ch_si_de), Some(ch_si_e)) = (p_si.comp_de(), p_si.comp_e()) {
            let (de60, e60) = analysis.si_thick_corr(&ch_si_de, &ch_si_e)?;

            if c_si_de60_e60_p.contains(e60, de60) {
                is_p = true;
            }
        }

        if is_p {
            for p_ic in &pack.ic {
                let c_tac_icst_si = analysis.get_cut_1d("tac_icst_si")?;
                if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                    println!("{}", p_si.time());
                    println!("{}", p_si);
                    println!(
                        "{:?}\n{:?}\n{:?}",
                        p_si.comp_de(),
                        p_si.comp_e(),
                        p_si.comp_be()
                    );
                    println!("{}", p_ic.time());
                    println!("{}", p_ic);
                    println!(
                        "{:?}\n{:?}\n{:?}\n{:?}",
                        p_ic.comp_x(),
                        p_ic.comp_y(),
                        p_ic.comp_de(),
                        p_ic.comp_e()
                    );
                    println!("====================")
                }
            }
        }
    }

    Ok(())
}
