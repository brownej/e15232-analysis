use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_ic))?;
    analysis.run()?;
    Ok(())
}

fn process_ic(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_ic in &pack.ic {
        // Get X & Y 1D spectra and X-E & Y-E 2D spectra
        // * For X, require Y strip = 16
        // * For Y, require X strip = 16
        if p_ic.all_x().len() == 1 && p_ic.all_y().len() == 1 {
            let h_x = &p_ic.all_x()[0];
            let h_y = &p_ic.all_y()[0];

            if h_y.detid.1 == 16 {
                analysis.fill_1d(
                    &format!("ic_x{:02}__y16", h_x.detid.1),
                    f64::from(h_x.value),
                )?;
                if p_ic.all_e().len() == 1 {
                    let h_e = &p_ic.all_e()[0];
                    analysis.fill_2d(
                        &format!("ic_x{:02}_e__y16", h_x.detid.1),
                        f64::from(h_e.value),
                        f64::from(h_x.value),
                    )?;
                }
            }
            if h_x.detid.1 == 16 {
                analysis.fill_1d(
                    &format!("ic_y{:02}__x16", h_y.detid.1),
                    f64::from(h_y.value),
                )?;
                if p_ic.all_e().len() == 1 {
                    let h_e = &p_ic.all_e()[0];
                    analysis.fill_2d(
                        &format!("ic_y{:02}_e__x16", h_y.detid.1),
                        f64::from(h_e.value),
                        f64::from(h_y.value),
                    )?;
                }
            }
        }

        // Get dE 1D spectrum and dE-E 2D spectrum
        if p_ic.all_de().len() == 1 {
            let h_de = &p_ic.all_de()[0];

            analysis.fill_1d("ic_de", f64::from(h_de.value))?;
            if p_ic.all_e().len() == 1 {
                let h_e = &p_ic.all_e()[0];
                analysis.fill_2d("ic_de_e", f64::from(h_e.value), f64::from(h_de.value))?;
            }
        }

        // Get E 1D spectrum
        if p_ic.all_e().len() == 1 {
            let h_e = &p_ic.all_e()[0];

            analysis.fill_1d("ic_e", f64::from(h_e.value))?;
        }
    }

    Ok(())
}
