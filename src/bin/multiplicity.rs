use e15232_analysis::{
    analysis::Analysis, angle::AngleInfo, errors::*, packet::PacketizedEvent, util::ic_angles,
};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().thickness.is_none() {
        Err("thickness file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    let c_ic_theta_beam = a.get_cut_1d("ic_theta_beam")?;
    let c_si_de60_e60_p = a.get_cut_2d("si_de60_e60_p")?;
    let c_si_de60_e60_a = a.get_cut_2d("si_de60_e60_a")?;
    let c_si_p_singles = a.get_cut_2d("si_p_singles")?;

    let mut mult_p = 0u32;
    let mut mult_a = 0u32;
    let mut mult_pa = 0u32;
    let mut mult_p_tac = 0u32;
    let mut mult_a_tac = 0u32;
    let mut mult_pa_tac = 0u32;

    for h_si in &pack.si {
        let mut tac = false;
        let mut is_beam = false;

        for h_ic in &pack.ic {
            tac = c_tac_icst_si.contains(h_ic.time() - h_si.time());

            let ic_pos_x = h_ic.comp_x().map(|ch| ch.detch);
            let ic_pos_y = h_ic.comp_y().map(|ch| ch.detch);
            if let (Some(p_x), Some(p_y)) = (ic_pos_x, ic_pos_y) {
                let angles = ic_angles(p_x, p_y);
                is_beam = c_ic_theta_beam.contains(angles.0);
            };
        }

        let mut is_p_tele = false;
        let mut is_p_single = false;
        let mut is_a_tele = false;
        //let mut is_a_single = false;

        match (h_si.comp_de(), h_si.comp_e()) {
            (Some(si_de), Some(si_e)) => {
                let (de60, e60) = a.si_thick_corr(&si_de, &si_e)?;
                is_p_tele = c_si_de60_e60_p.contains(e60, de60);
                is_a_tele = c_si_de60_e60_a.contains(e60, de60);
            }
            (None, Some(si_e)) => {
                let AngleInfo { theta_avg, .. } = a.get_angle_info(si_e.detnum, si_e.detch)?;

                // Singles only considered a proton if
                // * timing difference between ic and si is within gate
                // * ic is not in beam angular range
                is_p_single = c_si_p_singles.contains(theta_avg, si_e.energy) && tac && !is_beam;
            }
            _ => {}
        }
        let is_p = is_p_tele || is_p_single;
        //let is_a = is_a_tele || is_a_single;
        let is_a = is_a_tele;

        if is_p {
            mult_p += 1;
            if tac {
                mult_p_tac += 1;
            }
        }

        if is_a {
            mult_a += 1;
            if tac {
                mult_a_tac += 1;
            }
        }

        if is_p || is_a {
            mult_pa += 1;
            if tac {
                mult_pa_tac += 1;
            }
        }
    }

    a.fill_1d("multiplicity", pack.si.len() as f64)?;
    a.fill_1d("multiplicity__p", mult_p as f64)?;
    a.fill_1d("multiplicity__p_tac", mult_p_tac as f64)?;
    a.fill_1d("multiplicity__a", mult_a as f64)?;
    a.fill_1d("multiplicity__a_tac", mult_a_tac as f64)?;
    a.fill_1d("multiplicity__pa", mult_pa as f64)?;
    a.fill_1d("multiplicity__pa_tac", mult_pa_tac as f64)?;

    Ok(())
}
