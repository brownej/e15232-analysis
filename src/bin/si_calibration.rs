use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config".into())
    } else if a.get_config().hists.is_none() {
        Err("hists file not supplied in config".into())
    } else {
        Ok(())
    }
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for h in pack.all_hits() {
        let name = format!("val_{}_{}", h.detid.0, h.detid.1);
        if a.contains_hist(&name) {
            a.fill_1d(&name, f64::from(h.value))?;
        }
    }

    Ok(())
}
