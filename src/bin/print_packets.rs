/*
#![allow(non_snake_case)]
use datakiste::{event::Run, io::Datakiste, DetId};
use e15232_analysis::{angle::AngleInfo, errors::*, packet::PacketizedEvent};
use getopts::Options;
use std::{
    collections::HashMap,
    env,
    fs::File,
    io::{BufRead, BufReader},
};

struct Analysis {
    f_run_name: String,
    f_angle_name: String,
    angles: HashMap<DetId, AngleInfo>,
}

impl Analysis {
    //
    // Constructors
    //

    pub fn from_args() -> Result<Analysis> {
        // Parse the command line arguments
        let mut opts = Options::new();
        opts.optopt("a", "angles", "The angles file", "angle_file");
        let args: Vec<String> = env::args().collect();
        let matches = opts.parse(&args[1..])?;
        if matches.free.len() != 1 {
            panic!("Wrong number of args.");
        }

        // Create filenames
        let f_run_name = &matches.free[0];
        let f_angle_name = matches
            .opt_str("a")
            .unwrap_or_else(|| "angles.dat".to_string());

        Ok(Analysis {
            f_run_name: f_run_name.to_string(),
            f_angle_name: f_angle_name.to_string(),
            angles: HashMap::new(),
        })
    }

    //
    // Input Functions
    //

    fn read_angle_file(&mut self) -> Result<()> {
        let f_angle = BufReader::new(File::open(&self.f_angle_name)?);

        for l in f_angle.lines() {
            let l = l?;
            let x: Vec<_> = l.split_whitespace().collect();
            if x.is_empty() || x[0].starts_with('#') {
                continue;
            } else if x.len() < 8 {
                println!("WARNING: Error parsing a line in the angle file.");
            } else {
                let detid = x[0].parse::<u16>()?;
                let detch = x[1].parse::<u16>()?;

                let theta_min = x[2].parse::<f64>()?;
                let theta_max = x[3].parse::<f64>()?;
                let theta_avg = x[4].parse::<f64>()?;

                let phi_min = x[5].parse::<f64>()?;
                let phi_max = x[6].parse::<f64>()?;
                let phi_avg = x[7].parse::<f64>()?;

                let solid_angle = x[8].parse::<f64>()?;

                let v = self.angles.insert(
                    DetId(detid, detch),
                    AngleInfo {
                        theta_min,
                        theta_max,
                        theta_avg,
                        phi_min,
                        phi_max,
                        phi_avg,
                        solid_angle,
                    },
                );
                if v.is_some() {
                    println!(
                        "WARNING: There is already angle info for Det ID ({}, {}).",
                        detid, detch
                    );
                }
            }
        }

        Ok(())
    }

    fn read_run_file(&mut self) -> Result<Run> {
        let f_run = BufReader::new(File::open(&self.f_run_name)?);

        // Read in run from input file
        let mut dk: Datakiste = bincode::deserialize_from(f_run)?;
        let run = dk
            .items
            .swap_remove_index(0)
            .ok_or_else(|| format!("no items in {}", self.f_run_name))?
            .1
            .into_run()
            .ok_or_else(|| format!("first item in {} is not a run", self.f_run_name))?;
        Ok(run)
    }

    //
    // Analysis Function
    //

    pub fn run(&mut self) -> Result<()> {
        self.read_angle_file()
            .chain_err(|| "failed to read angle file")?;

        // Read in run from input file
        let run = self
            .read_run_file()
            .chain_err(|| "failed to read run file")?;

        // Main Loop
        for e in run.events {
            let pack = PacketizedEvent::new(e, self, &None);

            println!("{}", pack?);
            println!("====================");
        }

        Ok(())
    }
}

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.run()?;
    Ok(())
}
*/
fn main() {}
