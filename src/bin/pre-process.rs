#![feature(cell_update)]
#[macro_use]
extern crate lazy_static;
use datakiste::{
    calibration::{get_cal_map, Calibration},
    detector::Detector,
    error::Result,
    event::{Event, Hit, Run},
    get_dets, get_id_map,
    io::{Datakiste, DkItem},
    DaqId, DetId,
};
use std::{
    cell::Cell,
    collections::HashMap,
    fs::File,
    io::{BufReader, BufWriter},
    path::PathBuf,
};
use structopt::StructOpt;

lazy_static! {
    static ref OPTIONS: Options =
        Options::from_args().expect("error processing command line arguments");
}

#[derive(Debug, StructOpt)]
#[structopt(name = "pre-process", no_version)]
/// Pre-process datakiste run data
struct Opt {
    #[structopt(name = "INPUT_FILE", parse(from_os_str))]
    /// File to read
    f_in_name: PathBuf,
    #[structopt(name = "OUTPUT_FILE", parse(from_os_str))]
    /// File to write
    f_out_name: PathBuf,
    #[structopt(short = "h", long = "hagrid")]
    /// Include HAGRiD data
    hagrid: bool,
    #[structopt(short = "f", long = "fuzz")]
    /// Fuzz energies with random numbers
    fuzz: bool,
    #[structopt(name = "TIME", short = "b", long = "rebuild", use_delimiter = true)]
    /// Rebuild events, keeping only hits that are within TIME nanoseconds (before and after) of a silicon detector hit
    rebuild: Option<f64>,
    #[structopt(
        name = "CALIBRATION_FILE",
        short = "c",
        long = "calibration",
        parse(from_os_str)
    )]
    /// The energy calibration file
    f_cal_name: Option<PathBuf>,
    #[structopt(
        name = "DETECTOR_CONFIG_FILE",
        short = "d",
        long = "detector",
        parse(from_os_str)
    )]
    /// The detector configuration file
    f_det_name: Option<PathBuf>,
}

#[derive(Debug)]
struct Options {
    f_in_name: PathBuf,
    f_out_name: PathBuf,
    hagrid: bool,
    fuzz: bool,
    rebuild: Option<f64>,
    all_dets: Option<Vec<Detector>>,
    daq_det_map: Option<HashMap<DaqId, DetId>>,
    calib_map: Option<HashMap<DaqId, Calibration>>,
}

impl Options {
    fn from_args() -> Result<Self> {
        let Opt {
            f_in_name,
            f_out_name,
            hagrid,
            fuzz,
            rebuild,
            f_cal_name,
            f_det_name,
        } = Opt::from_args();
        let (all_dets, daq_det_map) = match f_det_name {
            Some(f_det_name) => {
                let f_det = BufReader::new(File::open(f_det_name)?);
                let all_dets = get_dets(f_det)?;
                let daq_det_map = get_id_map(&all_dets);
                (Some(all_dets), Some(daq_det_map))
            }
            None => (None, None),
        };
        let calib_map = match f_cal_name {
            Some(f_cal_name) => {
                let f_cal = BufReader::new(File::open(f_cal_name)?);
                Some(get_cal_map(f_cal)?)
            }
            None => None,
        };
        let rebuild = rebuild.map(|t| t.abs());
        Ok(Self {
            f_in_name,
            f_out_name,
            hagrid,
            fuzz,
            rebuild,
            all_dets,
            daq_det_map,
            calib_map,
        })
    }
}

fn main() -> Result<()> {
    let f_in = BufReader::new(File::open(&OPTIONS.f_in_name)?);
    let f_out = BufWriter::new(File::create(&OPTIONS.f_out_name)?);

    // Read in run from input file
    let dk: Datakiste = bincode::deserialize_from(f_in)?;

    let items = dk
        .into_iter()
        .filter_map(|(name, item)| {
            item.into_run().map(|run| {
                if let Some(time_window) = OPTIONS.rebuild {
                    (name, DkItem::from(process_run_rebuild(run, time_window)))
                } else {
                    (name, DkItem::from(process_run(run)))
                }
            })
        })
        .collect();

    let dk = Datakiste::with_items(items);
    // Write out to the output file
    bincode::serialize_into(f_out, &dk)?;

    Ok(())
}

fn process_run(run: Run) -> Run {
    let events = run
        .into_events()
        .map(|e| {
            let Event { hits } = e;
            let hits = hits.into_iter().filter_map(process_hit).collect();
            Event { hits }
        })
        .collect();
    Run { events }
}

fn process_run_rebuild(run: Run, time_window: f64) -> Run {
    struct HitUsed {
        hit: Hit,
        used: Cell<bool>,
    }

    let mut all_hits = run
        .into_hits()
        .filter_map(process_hit)
        .map(|hit| HitUsed {
            hit,
            used: Cell::new(false),
        })
        .collect::<Vec<_>>();

    // Sort hits by time.
    all_hits.sort_by(|a, b| a.hit.time.partial_cmp(&b.hit.time).unwrap());

    let mut events = Vec::new();

    // Because iterators are evaluated lazily, the check on whether a hit was used or not when
    // `next` is called
    let base_hits = all_hits.iter().enumerate().filter(|(_, h)| {
        // Only use silicon hits as base hits
        let is_si = if let Some(DetId(detnum, _detch)) = h.hit.detid {
            detnum >= 1 && detnum <= 38
        } else {
            false
        };
        // Only use bases that haven't already been used
        let is_used = h.used.get();

        is_si && !is_used
    });

    for (
        i,
        HitUsed {
            hit: base_hit,
            used: _base_used,
        },
    ) in base_hits
    {
        let mut hits = all_hits[..i]
            .iter()
            .rev()
            .take_while(
                |HitUsed {
                     hit: other_hit,
                     used: other_used,
                 }| {
                    let in_time_window = (base_hit.time - other_hit.time) < time_window;
                    other_used.update(|used| in_time_window && !used)
                },
            )
            .chain(all_hits[i..].iter().take_while(
                |HitUsed {
                     hit: other_hit,
                     used: other_used,
                 }| {
                    let in_time_window = (other_hit.time - base_hit.time) < time_window;
                    other_used.update(|used| in_time_window && !used)
                },
            ))
            .map(|HitUsed { hit, .. }| hit)
            .cloned()
            .collect::<Vec<_>>();
        hits.sort_by(|a, b| a.time.partial_cmp(&b.time).unwrap());

        if !hits.is_empty() {
            events.push(Event { hits });
        }
    }

    Run { events }
}

fn process_hit(mut hit: Hit) -> Option<Hit> {
    // This assigns detid and determines value from rawval
    if let (Some(all_dets), Some(daq_det_map)) =
        (OPTIONS.all_dets.as_ref(), OPTIONS.daq_det_map.as_ref())
    {
        hit.apply_det(all_dets, daq_det_map);
    }

    // Only include HAGRiD if flag was passed
    if let Some(DetId(detnum, _)) = hit.detid {
        if detnum == 43 && !OPTIONS.hagrid {
            return None;
        }
    }

    // Apply the calibration
    if let Some(calib_map) = OPTIONS.calib_map.as_ref() {
        // fuzz within bin limits if flag passed
        if OPTIONS.fuzz {
            hit.apply_calib_fuzz(calib_map);
        } else {
            hit.apply_calib(calib_map);
        }
    }
    Some(hit)
}
