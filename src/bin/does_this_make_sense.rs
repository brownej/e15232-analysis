use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_hits))?;
    analysis.run()?;
    Ok(())
}

fn process_hits(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let mut mult_p = 0u32;
    for p_si in &pack.si {
        let mut is_a = false;
        let mut is_p = false;
        let mut is_single = false;
        let c_si_de60_e60_p = analysis.get_cut_2d("si_de60_e60_p")?;
        let c_si_de60_e60_a = analysis.get_cut_2d("si_de60_e60_a")?;

        if let Some(ch_si_e) = p_si.comp_e() {
            match ch_si_e.detnum {
                1..=6 | 8 | 11 => {
                    is_single = true;
                }
                _ => {}
            }
        }

        if let (Some(ch_si_de), Some(ch_si_e)) = (p_si.comp_de(), p_si.comp_e()) {
            let (de60, e60) = analysis.si_thick_corr(&ch_si_de, &ch_si_e)?;
            if c_si_de60_e60_a.contains(e60, de60) {
                is_a = true;
            }
            if c_si_de60_e60_p.contains(e60, de60) {
                is_p = true;
                mult_p += 1;
            }
            analysis.fill_2d("si_de60_e60", e60, de60)?;
            if e60 > 100. && de60 > 100. {
                analysis.push_2d("si_de60_e60", e60, de60)?;
            }
        }

        if is_single {
            let mut has_tac = false;
            let ch_si_e = p_si.comp_e().unwrap();
            let e = ch_si_e.energy;

            let theta = analysis
                .get_angle_info(ch_si_e.detnum, ch_si_e.detch)?
                .theta_avg;

            for p_ic in &pack.ic {
                let c_tac_icst_si = analysis.get_cut_1d("tac_icst_si")?;
                if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                    has_tac = true;
                }
            }
            if has_tac {
                analysis.push_2d("si_e_angle__single_tac", theta, e)?;
            }
        }

        if is_a {
            let mut has_tac = false;
            let ch_si_de = p_si.comp_de().unwrap();
            let ch_si_e = p_si.comp_e().unwrap();

            let (de60, e60) = analysis.si_thick_corr(&ch_si_de, &ch_si_e)?;

            let theta = analysis
                .get_angle_info(ch_si_e.detnum, ch_si_e.detch)?
                .theta_avg;

            for p_ic in &pack.ic {
                let c_tac_icst_si = analysis.get_cut_1d("tac_icst_si")?;
                if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                    has_tac = true;
                }
            }
            analysis.push_2d("si_e_angle__a", theta, de60 + e60)?;
            analysis.push_2d("si_de_e__a", e60, de60)?;
            if has_tac {
                analysis.push_2d("si_e_angle__a_tac", theta, de60 + e60)?;
                analysis.push_2d("si_de_e__a_tac", e60, de60)?;
            }
        }

        if is_p {
            let mut mult_ic_p = (pack.ic.len() as u32, 0u32, 0u32, 0u32, 0u32);
            let mut mult_ic_p_tac = (0u32, 0u32, 0u32, 0u32, 0u32);
            let mut y_count_x = 0u32;
            let mut de_count_xy = 0u32;
            let mut e_count_xyde = 0u32;
            let mut de_count_y = 0u32;
            let mut e_count_yde = 0u32;
            let mut any_count = 0u32;

            for p_ic in &pack.ic {
                let c_tac_icst_si = analysis.get_cut_1d("tac_icst_si")?;
                if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                    analysis.push_4d(
                        "IC_multiplicities",
                        p_ic.all_x().len() as f64,
                        p_ic.all_y().len() as f64,
                        p_ic.all_de().len() as f64,
                        p_ic.all_e().len() as f64,
                    )?;

                    mult_ic_p_tac.0 += 1;
                    if !p_ic.all_x().is_empty() {
                        mult_ic_p_tac.1 += 1;
                    }
                    if !p_ic.all_y().is_empty() {
                        mult_ic_p_tac.2 += 1;
                    }
                    if !p_ic.all_de().is_empty() {
                        mult_ic_p_tac.3 += 1;
                    }
                    if !p_ic.all_e().is_empty() {
                        mult_ic_p_tac.4 += 1;
                    }

                    if !p_ic.all_x().is_empty() && !p_ic.all_y().is_empty() {
                        y_count_x += 1;
                    }
                    if !p_ic.all_x().is_empty()
                        && !p_ic.all_y().is_empty()
                        && !p_ic.all_de().is_empty()
                    {
                        de_count_xy += 1;
                    }
                    if !p_ic.all_x().is_empty()
                        && !p_ic.all_y().is_empty()
                        && !p_ic.all_de().is_empty()
                        && !p_ic.all_e().is_empty()
                    {
                        e_count_xyde += 1;
                    }
                    if !p_ic.all_y().is_empty() && !p_ic.all_de().is_empty() {
                        de_count_y += 1;
                    }
                    if !p_ic.all_y().is_empty()
                        && !p_ic.all_de().is_empty()
                        && !p_ic.all_e().is_empty()
                    {
                        e_count_yde += 1;
                    }
                    if !p_ic.all_x().is_empty()
                        || !p_ic.all_y().is_empty()
                        || !p_ic.all_de().is_empty()
                        || !p_ic.all_e().is_empty()
                    {
                        any_count += 1;
                    }
                }
                if !p_ic.all_x().is_empty() {
                    mult_ic_p.1 += 1;
                }
                if !p_ic.all_y().is_empty() {
                    mult_ic_p.2 += 1;
                }
                if !p_ic.all_de().is_empty() {
                    mult_ic_p.3 += 1;
                }
                if !p_ic.all_e().is_empty() {
                    mult_ic_p.4 += 1;
                }

                analysis.fill_1d("tac__p", p_ic.time() - p_si.time())?;
            }

            let ch_si_de = p_si.comp_de().unwrap();
            let ch_si_e = p_si.comp_e().unwrap();

            let (de60, e60) = analysis.si_thick_corr(&ch_si_de, &ch_si_e)?;

            let theta = analysis
                .get_angle_info(ch_si_e.detnum, ch_si_e.detch)?
                .theta_avg;

            analysis.push_2d("si_e_angle__p", theta, de60 + e60)?;
            analysis.push_2d("si_de_e__p", e60, de60)?;
            analysis.fill_1d("proton_counts", 1.0)?;
            if mult_ic_p.0 > 0 {
                analysis.fill_1d("proton_counts", 2.0)?;
            }
            if mult_ic_p_tac.0 > 0 {
                analysis.push_2d("si_e_angle__p_tac", theta, de60 + e60)?;
                analysis.push_2d("si_de_e__p_tac", e60, de60)?;
                analysis.fill_1d("proton_counts", 3.0)?;
            }

            analysis.fill_1d("ic_mult_p", f64::from(mult_ic_p.0))?;
            analysis.fill_1d("icx_mult_p", f64::from(mult_ic_p.1))?;
            analysis.fill_1d("icy_mult_p", f64::from(mult_ic_p.2))?;
            analysis.fill_1d("icde_mult_p", f64::from(mult_ic_p.3))?;
            analysis.fill_1d("ice_mult_p", f64::from(mult_ic_p.4))?;
            analysis.fill_1d("ic_mult_p_tac", f64::from(mult_ic_p_tac.0))?;
            analysis.fill_1d("icx_mult_p_tac", f64::from(mult_ic_p_tac.1))?;
            analysis.fill_1d("icy_mult_p_tac", f64::from(mult_ic_p_tac.2))?;
            analysis.fill_1d("icde_mult_p_tac", f64::from(mult_ic_p_tac.3))?;
            analysis.fill_1d("ice_mult_p_tac", f64::from(mult_ic_p_tac.4))?;
            analysis.fill_1d("icy_mult_p_prev_tac", f64::from(y_count_x))?;
            analysis.fill_1d("icde_mult_p_prev_tac", f64::from(de_count_xy))?;
            analysis.fill_1d("icde_mult_p_prev_nox_tac", f64::from(de_count_y))?;
            analysis.fill_1d("ice_mult_p_prev_tac", f64::from(e_count_xyde))?;
            analysis.fill_1d("ice_mult_p_prev_nox_tac", f64::from(e_count_yde))?;
            analysis.fill_1d("icany_mult_p_tac", f64::from(any_count))?;
        }
    }

    analysis.fill_1d("proton_mult", f64::from(mult_p))?;

    Ok(())
}
