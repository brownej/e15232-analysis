#![allow(non_snake_case)]
use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_run_func(Box::new(process_si__ic))?;
    analysis.run()?;
    Ok(())
}

fn process_si__ic(analysis: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_ic in &pack.ic {
        if let (Some(ch_x), Some(ch_y)) = (p_ic.comp_x(), p_ic.comp_y()) {
            let mut is_recoil = false;

            let e_x = ch_x.energy;
            let e_y = ch_y.energy;

            let low_corr = 900. / 1200.;
            let high_corr = 1400. / 1200.;

            let e_corr = analysis.ic_37k_corr(Some(e_x), Some(e_y), None, None)?;
            if let (Some(e15_x), Some(e15_y), _, _) = e_corr {
                if e15_x > low_corr * 5293.2
                    && e15_x < high_corr * 6823.8
                    && e15_y > low_corr * 5349.8
                    && e15_y < high_corr * 6334.0
                {
                    is_recoil = true;
                }
            }
            let e_corr = analysis.ic_37ar_corr(Some(e_x), Some(e_y), None, None)?;
            if let (Some(e15_x), Some(e15_y), _, _) = e_corr {
                if e15_x > low_corr * 5293.2
                    && e15_x < high_corr * 6823.8
                    && e15_y > low_corr * 5349.8
                    && e15_y < high_corr * 6334.0
                {
                    is_recoil = true;
                }
            }
            let e_corr = analysis.ic_37cl_corr(Some(e_x), Some(e_y), None, None)?;
            if let (Some(e15_x), Some(e15_y), _, _) = e_corr {
                if e15_x > low_corr * 5293.2
                    && e15_x < high_corr * 6823.8
                    && e15_y > low_corr * 5349.8
                    && e15_y < high_corr * 6334.0
                {
                    is_recoil = true;
                }
            }
            if is_recoil {
                for p_si in &pack.si {
                    let c_tac_icst_si = analysis.get_cut_1d("tac_icst_si")?;
                    if c_tac_icst_si.contains(p_ic.time() - p_si.time()) {
                        match (p_si.comp_de(), p_si.comp_e()) {
                            (Some(ch_de), Some(ch_e)) => {
                                let theta =
                                    analysis.get_angle_info(ch_e.detnum, ch_e.detch)?.theta_avg;

                                analysis.push_2d(
                                    "si_e_angle__recoil_de_e",
                                    theta,
                                    ch_de.energy + ch_e.energy,
                                )?;
                            }
                            (None, Some(ch_e)) => {
                                let theta =
                                    analysis.get_angle_info(ch_e.detnum, ch_e.detch)?.theta_avg;

                                analysis.fill_2d("si_e_angle__recoil_e", theta, ch_e.energy)?;
                                analysis.push_2d("si_e_angle__recoil_e", theta, ch_e.energy)?;
                            }
                            _ => {}
                        }
                    }
                }
            }
        }
    }

    Ok(())
}
