use e15232_analysis::{analysis::Analysis, angle::AngleInfo, errors::*, packet::PacketizedEvent};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for h_si in &pack.si {
        let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
        let mut tac = false;
        for h_ic in &pack.ic {
            tac = c_tac_icst_si.contains(h_ic.time() - h_si.time());
        }

        if let (Some(si_de), Some(si_e)) = (h_si.comp_de(), h_si.comp_e()) {
            let (de60, e60) = a.si_thick_corr(&si_de, &si_e)?;
            a.fill_2d("si_e60_de60", e60, de60)?;
            if tac {
                a.fill_2d("si_e60_de60__tac", e60, de60)?;
            }
        }

        let (theta_avg, e_tot) = match (h_si.comp_de(), h_si.comp_e()) {
            (Some(si_de), Some(si_e)) => {
                let AngleInfo { theta_avg, .. } =
                    a.get_angle_info(si_e.detnum, si_e.detch as f64)?;
                let e_tot = si_de.energy + si_e.energy;
                (theta_avg, e_tot)
            }
            (Some(si_de), None) => {
                let AngleInfo { theta_avg, .. } =
                    a.get_angle_info(si_de.detnum, si_de.detch as f64)?;
                (theta_avg, si_de.energy)
            }
            (None, Some(si_e)) => {
                let AngleInfo { theta_avg, .. } =
                    a.get_angle_info(si_e.detnum, si_e.detch as f64)?;
                (theta_avg, si_e.energy)
            }
            (None, None) => continue,
        };
        a.fill_2d("theta_etot", theta_avg, e_tot)?;
        if tac {
            a.fill_2d("theta_etot__tac", theta_avg, e_tot)?;
        }
    }

    Ok(())
}
