use datakiste::{
    event::{Event, Hit, Run},
    io::Datakiste,
};
use e15232_analysis::{
    analysis::Analysis,
    errors::*,
    packet::PacketizedEvent,
    util::{is_si_alpha, is_si_proton, is_si_single},
};
use getopts::Options;
use std::{
    env,
    fs::File,
    io::BufWriter,
    iter::{empty, once},
    path::Path,
};

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis
        .other_data
        .borrow_mut()
        .insert("events".to_string(), Box::new(Vec::<Event>::new()));
    analysis.register_run_func(Box::new(process))?;
    analysis.register_end_func(Box::new(write_run))?;
    analysis.run()?;
    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    let c_tac_icst_si = a.get_cut_1d("tac_icst_si")?;
    for p_ic in &pack.ic {
        for p_si in &pack.si {
            if (c_tac_icst_si.contains(p_ic.time() - p_si.time()))
                && (is_si_proton(&a, &p_si)? || is_si_alpha(&a, &p_si)? || is_si_single(&p_si)?)
            {
                let mut hits = empty()
                    .chain(p_ic.all_x())
                    .chain(p_ic.all_y())
                    .chain(p_ic.all_de())
                    .chain(p_ic.all_e())
                    .chain(p_si.all_de())
                    .chain(p_si.all_e())
                    .chain(p_si.all_be())
                    .map(|x| x.into())
                    .collect::<Vec<Hit>>();
                hits.sort_by(|a, b| a.time.partial_cmp(&b.time).unwrap());

                a.other_data
                    .borrow_mut()
                    .get_mut("events")
                    .ok_or("other_data does not have `events`")?
                    .downcast_mut::<Vec<Event>>()
                    .ok_or("`events` is not a `Vec<Event>`")?
                    .push(Event { hits });
            }
        }
    }

    Ok(())
}

fn write_run(a: &Analysis) -> Result<()> {
    let events = a
        .other_data
        .borrow_mut()
        .get_mut("events")
        .ok_or("other_data does not have `events`")?
        .downcast_mut::<Vec<Event>>()
        .ok_or("`events` is not a `Vec<Event>`")?
        .clone();

    if !events.is_empty() {
        let mut opts = Options::new();
        opts.optopt("a", "angles", "The angles file", "angle_file");
        opts.optopt("c", "cuts", "The cuts file", "cut_file");
        opts.optopt("t", "thick", "The thickness data file", "thick_file");
        opts.optopt("h", "hists", "The histograms file", "hist_file");
        opts.optopt("s", "sums", "The sum histograms file", "sum_hist_file");
        opts.optopt("r", "run_info", "The run info file", "run_inf_file");

        let args: Vec<String> = env::args().collect();
        let matches = opts.parse(&args[1..])?;
        let f_run_name = &matches.free[0];
        let base = Path::new(&f_run_name);
        let base = base.file_stem().ok_or("failed to get file stem")?;
        let base = base
            .to_str()
            .ok_or("failed to convert base to str")?
            .to_string();
        let f_run_out_name = &format!("{}.dkr", base);

        let f_run_out = BufWriter::new(File::create(f_run_out_name)?);

        let run = Run { events };
        let items = once((base, run.into())).collect();
        let dk = Datakiste::with_items(items);
        bincode::serialize_into(f_run_out, &dk)?;
    }

    Ok(())
}
