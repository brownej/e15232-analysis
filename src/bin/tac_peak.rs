use e15232_analysis::analysis::Analysis;
use e15232_analysis::errors::*;
use e15232_analysis::packet::PacketizedEvent;

fn main() {
    if let Err(ref e) = main_catch_err() {
        eprintln!("ERROR: {}", e);
        for e in e.iter().skip(1) {
            eprintln!("Caused by: {}", e);
        }
        if let Some(backtrace) = e.backtrace() {
            eprintln!("Backtrace: {:?}", backtrace);
        }
        std::process::exit(1);
    }
}

fn main_catch_err() -> Result<()> {
    let mut analysis = Analysis::from_args()?;
    analysis.register_start_func(Box::new(check_input))?;
    analysis.register_run_func(Box::new(process))?;
    analysis.run()?;
    Ok(())
}

fn check_input(a: &Analysis) -> Result<()> {
    if a.get_config().angle.is_none() {
        Err("angle file not supplied in config")?
    }
    if a.get_config().cut.is_none() {
        Err("cut file not supplied in config")?
    }
    if a.get_config().hists.is_none() {
        Err("hists file not supplied in config")?
    }

    Ok(())
}

fn process(a: &Analysis, pack: &PacketizedEvent) -> Result<()> {
    for p_si in &pack.si {
        if let Some(ch_e) = p_si.comp_e() {
            if ch_e.detnum == 8 || ch_e.detnum == 11 {
                let theta = a.get_angle_info(ch_e.detnum, ch_e.detch)?.theta_avg;
                a.fill_2d("e_th", theta, ch_e.energy)?;
                let c_sorr_dn_e_angle_a = a.get_cut_2d("sorr_dn_e_angle_a")?;
                for p_ic in &pack.ic {
                    a.fill_1d("tac", p_ic.time() - p_si.time())?;
                    if c_sorr_dn_e_angle_a.contains(theta, ch_e.energy) {
                        a.fill_1d("tac__a", p_ic.time() - p_si.time())?;
                    }
                }
            }
        }
    }

    Ok(())
}
