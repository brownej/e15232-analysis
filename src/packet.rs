use crate::{analysis::Analysis, angle::AngleInfo, errors::*, util::Hit};
use datakiste::{self, event::Event, DetId};
use std::{cell::Cell, collections::HashMap, convert::TryInto, fmt};

// TODO: Use ValUnc
#[derive(Debug, Clone, Copy)]
pub struct CompositeHit {
    pub detnum: u16,
    pub detch: f64,
    pub energy: f64,
    pub time: f64,
}

impl CompositeHit {
    pub fn new(v: &[Hit]) -> Option<CompositeHit> {
        if v.is_empty() {
            None
        } else {
            let mut detnum = 0;
            let mut energy = 0.0;
            let mut time = 0.0;
            let mut max = (0.0, 0.0);

            for h in v {
                // FIXME: handle None
                let hit_detid = h.detid;
                let hit_energy = h.energy.val;
                // FIXME: This just overwrites previous vals. Do we need it?
                detnum = hit_detid.0;
                // TODO: weighted detch? the following doesn't work, though
                //detch += h.energy * (h.detid.1 as f64); // This is weighted based on energy
                if hit_energy > max.0 {
                    max = (hit_energy, f64::from(hit_detid.1));
                }
                energy += hit_energy;
                time += h.time; // FIXME: This takes the simple average of the times. Do we need it?
            }

            time /= v.len() as f64;

            Some(CompositeHit {
                detnum,
                detch: max.1,
                energy,
                time,
            })
        }
    }
}

#[derive(Debug)]
pub struct PacketizedEvent {
    pub ic: Vec<IcPacket>,
    pub si: Vec<SiPacket>,
    pub other: Vec<OtherPacket>,
}

impl PacketizedEvent {
    // This function takes an event and splits the hits into packets.
    //
    // It creates packets and adds together the energy from multiple
    // channels of the same section, using all other hit information
    // from the hit with the highest energy.
    //
    // Each section has its own timing offset and uncertainty (these
    // were determined by looking at the data). Using these offsets
    // and uncertainties, we determine how "fit" a hit is for that
    // packet. The hit is put into the packet with the highest
    // fitness, or a new packet is created if its fitness for all
    // other packets is below the threshold.
    pub fn new(
        e: Event,
        analysis: &Analysis,
        hit_acceptance: &Option<Box<dyn Fn(&Analysis, &Hit) -> bool>>,
    ) -> Result<PacketizedEvent> {
        use self::Packet::*;

        let mut packets = Vec::<Packet>::new();
        let thresh = -5.;

        let angles = analysis.get_all_angle_info()?;

        for h in e.hits {
            let h: Hit = h.try_into()?;
            let mut accepted = true;
            if let Some(ref hit_acceptance) = *hit_acceptance {
                accepted = hit_acceptance(analysis, &h);
            }

            if accepted {
                let mut best: Option<(usize, f64)> = None;
                for (i, pack) in packets.iter().enumerate() {
                    let fit = pack.fitness(&h, angles);

                    match best {
                        Some((_, best_fit)) if fit <= best_fit => {}
                        _ => best = Some((i, fit)),
                    }
                }

                match best {
                    Some((best_i, best_fit)) if best_fit > thresh => {
                        packets[best_i].insert(h.clone());
                    }
                    _ => {
                        let pack = Packet::new(h.clone());
                        packets.push(pack);
                    }
                }
            }
        }

        let mut ic = vec![];
        let mut si = vec![];
        let mut other = vec![];

        for p in packets {
            match p {
                Ic(p) => {
                    ic.push(p);
                }
                Si(p) => {
                    si.push(p);
                }
                Other(p) => {
                    other.push(p);
                }
            }
        }

        Ok(PacketizedEvent { ic, si, other })
    }

    pub fn all_hits(&self) -> impl Iterator<Item = &Hit> {
        self.ic
            .iter()
            .map(|x| {
                x.all_x()
                    .iter()
                    .chain(x.all_y().iter())
                    .chain(x.all_de().iter())
                    .chain(x.all_e().iter())
            })
            .flatten()
            .chain(
                self.si
                    .iter()
                    .map(|x| x.all_de().iter().chain(x.all_e().iter()))
                    .flatten(),
            )
            .chain(self.other.iter().map(|x| x.all_hits().iter()).flatten())
    }
}

impl fmt::Display for PacketizedEvent {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for p in &self.ic {
            writeln!(f, "IC:")?;
            writeln!(f, "{}", p)?;
        }
        for p in &self.si {
            writeln!(f, "Si:")?;
            writeln!(f, "{}", p)?;
        }
        for p in &self.other {
            writeln!(f, "Other:")?;
            writeln!(f, "{}", p)?;
        }
        Ok(())
    }
}

#[derive(Debug)]
enum Packet {
    Ic(IcPacket),
    Si(SiPacket),
    Other(OtherPacket),
}

impl Packet {
    fn new(hit: Hit) -> Packet {
        use self::Packet::*;

        let detnum = hit.detid.0;
        match detnum {
            39..=42 => Ic(IcPacket::new(hit)),
            1..=38 => Si(SiPacket::new(hit)),
            _ => Other(OtherPacket::new(hit)),
        }
    }

    fn insert(&mut self, hit: Hit) {
        use self::Packet::*;

        match *self {
            Ic(ref mut p) => p.insert(hit),
            Si(ref mut p) => p.insert(hit),
            Other(ref mut p) => p.insert(hit),
        }
    }

    fn fitness(&self, hit: &Hit, angles: &HashMap<DetId, AngleInfo>) -> f64 {
        use self::Packet::*;

        match *self {
            Ic(ref p) => p.fitness(hit, angles),
            Si(ref p) => p.fitness(hit, angles),
            Other(ref p) => p.fitness(hit, angles),
        }
    }
}

#[derive(Debug)]
pub struct IcPacket {
    x: Vec<Hit>,
    y: Vec<Hit>,
    de: Vec<Hit>,
    e: Vec<Hit>,
    time: Cell<Option<f64>>,
    comp_x: Cell<Option<CompositeHit>>,
    comp_y: Cell<Option<CompositeHit>>,
    comp_de: Cell<Option<CompositeHit>>,
    comp_e: Cell<Option<CompositeHit>>,
}

impl IcPacket {
    fn new(hit: Hit) -> IcPacket {
        let mut p = IcPacket {
            x: Vec::new(),
            y: Vec::new(),
            de: Vec::new(),
            e: Vec::new(),
            time: Cell::new(None),
            comp_x: Cell::new(None),
            comp_y: Cell::new(None),
            comp_de: Cell::new(None),
            comp_e: Cell::new(None),
        };
        p.insert(hit);
        p
    }

    fn insert(&mut self, hit: Hit) {
        let detnum = hit.detid.0;
        match detnum {
            39 => {
                self.x.push(hit);
                self.comp_x.set(None);
            }
            40 => {
                self.y.push(hit);
                self.comp_y.set(None);
            }
            41 => {
                self.de.push(hit);
                self.comp_de.set(None);
            }
            42 => {
                self.e.push(hit);
                self.comp_e.set(None);
            }
            _ => {
                panic!("IcPacket was given a non-IC hit");
            }
        }
        self.time.set(None);
    }

    fn fitness(&self, hit: &Hit, _angles: &HashMap<DetId, AngleInfo>) -> f64 {
        let time_windows = [(23., 163.), (23., 165.), (409., 44.), (718., 99.)];
        let mut fitness = 0.;

        let detnum = hit.detid.0;
        match detnum {
            i @ 39..=42 => {
                let t_win = time_windows[i as usize - 39];
                fitness -= (((hit.time - self.time()) - t_win.0) / (t_win.1)).abs();
            }
            _ => {
                fitness = ::std::f64::NEG_INFINITY;
            }
        }
        fitness
    }

    pub fn all_x(&self) -> &Vec<Hit> {
        &self.x
    }

    pub fn all_y(&self) -> &Vec<Hit> {
        &self.y
    }

    pub fn all_de(&self) -> &Vec<Hit> {
        &self.de
    }

    pub fn all_e(&self) -> &Vec<Hit> {
        &self.e
    }

    pub fn time(&self) -> f64 {
        if self.time.get().is_none() {
            self.compute_time();
        }
        self.time
            .get()
            .expect("IcPacket doesn't have any time data")
    }

    pub fn comp_x(&self) -> Option<CompositeHit> {
        if self.comp_x.get().is_none() {
            self.compute_comp_x();
        }
        self.comp_x.get()
    }

    pub fn comp_y(&self) -> Option<CompositeHit> {
        if self.comp_y.get().is_none() {
            self.compute_comp_y();
        }
        self.comp_y.get()
    }

    pub fn comp_de(&self) -> Option<CompositeHit> {
        if self.comp_de.get().is_none() {
            self.compute_comp_de();
        }
        self.comp_de.get()
    }

    pub fn comp_e(&self) -> Option<CompositeHit> {
        if self.comp_e.get().is_none() {
            self.compute_comp_e();
        }
        self.comp_e.get()
    }

    fn compute_time(&self) {
        //let time_offsets = [0., 0., 386., 695.];
        let time_offsets = (0., 0., 662., 1280.);
        self.time.set(self.time.get().or_else(|| {
            self.x
                .iter()
                .map(|x| x.time - time_offsets.0)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
        self.time.set(self.time.get().or_else(|| {
            self.y
                .iter()
                .map(|x| x.time - time_offsets.0)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
        self.time.set(self.time.get().or_else(|| {
            self.de
                .iter()
                .map(|x| x.time - time_offsets.0)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
        self.time.set(self.time.get().or_else(|| {
            self.e
                .iter()
                .map(|x| x.time - time_offsets.0)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
    }

    fn compute_comp_x(&self) {
        self.comp_x.set(CompositeHit::new(self.all_x()));
    }

    fn compute_comp_y(&self) {
        self.comp_y.set(CompositeHit::new(self.all_y()));
    }

    fn compute_comp_de(&self) {
        self.comp_de.set(CompositeHit::new(self.all_de()));
    }

    fn compute_comp_e(&self) {
        self.comp_e.set(CompositeHit::new(self.all_e()));
    }
}

impl fmt::Display for IcPacket {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for h in self.all_x() {
            writeln!(
                f,
                "# X {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        for h in self.all_y() {
            writeln!(
                f,
                "# Y {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        for h in self.all_de() {
            writeln!(
                f,
                "# dE {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        for h in self.all_e() {
            writeln!(
                f,
                "# E {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        Ok(())
    }
}

#[allow(non_camel_case_types)]
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum SiDetType {
    dE,
    E,
    bE,
    OtherType,
}

fn si_detid_to_det_type(i: u16) -> SiDetType {
    use self::SiDetType::*;

    match i {
        35..=38 | 14 | 16 | 18 | 20 | 22 => dE,
        1..=12 | 13 | 15 | 17 | 19 | 21 => E,
        23..=34 => bE,
        _ => OtherType,
    }
}

fn si_detid_to_detno(i: u16) -> Option<u16> {
    match i {
        35 => Some(8),
        36 => Some(10),
        37 => Some(11),
        38 => Some(14),
        13 | 14 => Some(15),
        15 | 16 => Some(16),
        17 | 18 => Some(17),
        19 | 20 => Some(18),
        21 | 22 => Some(19),
        n @ 1..=4 => Some(n),
        n @ 5..=10 => Some(n + 1),
        n @ 11..=12 => Some(n + 2),
        n @ 23..=26 => Some(n - 22),
        n @ 27..=32 => Some(n - 21),
        n @ 33..=34 => Some(n - 20),
        _ => None,
    }
}

#[derive(Debug)]
pub struct SiPacket {
    detno: u16,
    de: Vec<Hit>,
    e: Vec<Hit>,
    be: Vec<Hit>,
    time: Cell<Option<f64>>,
    comp_de: Cell<Option<CompositeHit>>,
    comp_e: Cell<Option<CompositeHit>>,
    comp_be: Cell<Option<CompositeHit>>,
}

impl SiPacket {
    fn new(hit: Hit) -> SiPacket {
        let detno =
            si_detid_to_detno(hit.detid.0).expect("trying to create SiPacket with no detno");
        let mut p = SiPacket {
            detno,
            de: Vec::new(),
            e: Vec::new(),
            be: Vec::new(),
            time: Cell::new(None),
            comp_de: Cell::new(None),
            comp_e: Cell::new(None),
            comp_be: Cell::new(None),
        };
        p.insert(hit);
        p
    }

    fn insert(&mut self, hit: Hit) {
        use self::SiDetType::*;

        let det_type = si_detid_to_det_type(hit.detid.0);
        match det_type {
            dE => {
                self.de.push(hit);
                self.comp_de.set(None);
            }
            E => {
                self.e.push(hit);
                self.comp_e.set(None);
            }
            bE => {
                self.be.push(hit);
                self.comp_be.set(None);
            }
            _ => {
                panic!("SiPacket was given a non-Si hit");
            }
        };
        self.time.set(None);
    }

    fn fitness(&self, hit: &Hit, _angles: &HashMap<DetId, AngleInfo>) -> f64 {
        use self::SiDetType::*;

        let det_type = si_detid_to_det_type(hit.detid.0);
        let detno = si_detid_to_detno(hit.detid.0);

        if (hit.time - self.time()).abs() > ::std::f64::EPSILON {
            return ::std::f64::NEG_INFINITY;
        }
        if detno.is_none() {
            return ::std::f64::NEG_INFINITY;
        }

        let mut fitness = 0.0;
        let detno = detno.expect("detno is None when it was aleady checked");

        match det_type {
            dE if detno == self.detno => {}
            E if detno == self.detno => {}
            bE if detno == self.detno => {}
            _ => {
                fitness = ::std::f64::NEG_INFINITY;
            }
        }

        fitness
    }

    pub fn all_de(&self) -> &Vec<Hit> {
        &self.de
    }

    pub fn all_e(&self) -> &Vec<Hit> {
        &self.e
    }

    pub fn all_be(&self) -> &Vec<Hit> {
        &self.be
    }

    pub fn detno(&self) -> u16 {
        self.detno
    }

    pub fn time(&self) -> f64 {
        if self.time.get().is_none() {
            self.compute_time();
        }
        self.time
            .get()
            .expect("SiPacket doesn't have any time data")
    }

    pub fn comp_de(&self) -> Option<CompositeHit> {
        if self.comp_de.get().is_none() {
            self.compute_comp_de();
        }
        self.comp_de.get()
    }

    pub fn comp_e(&self) -> Option<CompositeHit> {
        if self.comp_e.get().is_none() {
            self.compute_comp_e();
        }
        self.comp_e.get()
    }

    pub fn comp_be(&self) -> Option<CompositeHit> {
        if self.comp_be.get().is_none() {
            self.compute_comp_be();
        }
        self.comp_be.get()
    }

    fn compute_time(&self) {
        self.time.set(self.time.get().or_else(|| {
            self.de
                .iter()
                .map(|x| x.time)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
        self.time.set(self.time.get().or_else(|| {
            self.e
                .iter()
                .map(|x| x.time)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
        self.time.set(self.time.get().or_else(|| {
            self.be
                .iter()
                .map(|x| x.time)
                .min_by(|a, b| a.partial_cmp(b).unwrap())
        }));
    }

    fn compute_comp_de(&self) {
        self.comp_de.set(CompositeHit::new(self.all_de()));
    }

    fn compute_comp_e(&self) {
        self.comp_e.set(CompositeHit::new(self.all_e()));
    }

    fn compute_comp_be(&self) {
        self.comp_be.set(CompositeHit::new(self.all_be()));
    }
}

impl fmt::Display for SiPacket {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for h in self.all_de() {
            writeln!(
                f,
                "# dE {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        for h in self.all_e() {
            writeln!(
                f,
                "# E {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        for h in self.all_be() {
            writeln!(
                f,
                "# bE {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub struct OtherPacket {
    hits: Vec<Hit>,
    time: Cell<Option<f64>>,
    comp_hit: Cell<Option<CompositeHit>>,
}

impl OtherPacket {
    fn new(hit: Hit) -> OtherPacket {
        let mut p = OtherPacket {
            hits: Vec::new(),
            time: Cell::new(None),
            comp_hit: Cell::new(None),
        };
        p.insert(hit);
        p
    }

    fn insert(&mut self, hit: Hit) {
        if self.hits.is_empty() {
            self.hits.push(hit);
        } else {
            panic!("OtherPacket was given a second hit");
        }
    }

    fn fitness(&self, _hit: &Hit, _angles: &HashMap<DetId, AngleInfo>) -> f64 {
        ::std::f64::NEG_INFINITY
    }

    pub fn all_hits(&self) -> &Vec<Hit> {
        &self.hits
    }

    pub fn time(&self) -> f64 {
        if self.time.get().is_none() {
            self.compute_time();
        }
        self.time
            .get()
            .expect("SiPacket doesn't have any time data")
    }

    pub fn comp_hit(&self) -> Option<CompositeHit> {
        if self.comp_hit.get().is_none() {
            self.compute_comp_hit();
        }
        self.comp_hit.get()
    }

    fn compute_time(&self) {
        self.time.set(
            self.hits
                .iter()
                .map(|x| x.time)
                .min_by(|a, b| a.partial_cmp(b).unwrap()),
        );
    }

    fn compute_comp_hit(&self) {
        self.comp_hit.set(CompositeHit::new(self.all_hits()));
    }
}

impl fmt::Display for OtherPacket {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for h in self.all_hits() {
            writeln!(
                f,
                "# {:?} {} {} {} {} {}",
                h.detid,
                h.rawval,
                h.energy.val,
                (h.energy.unc.0).0,
                (h.energy.unc.1).0,
                h.time
            )?;
        }
        Ok(())
    }
}
