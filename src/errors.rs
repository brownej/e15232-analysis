#![allow(deprecated)]
error_chain! {
    foreign_links{
        Opts(::getopts::Fail);
        Int(::std::num::ParseIntError);
        Float(::std::num::ParseFloatError);
        Io(::std::io::Error) #[cfg(unix)];
        Json(::serde_json::Error);
        Bincode(bincode::Error);
    }
    errors {
        MissingRunInfo(run_name: String) {
            description("run info missing")
            display("run info missing for '{}'", run_name)
        }
        HitConversionError {
            description("error converting from datakiste::Hit to e15232_analysis::Hit")
            display("error converting from datakiste::Hit to e15232_analysis::Hit")
        }
    }
}
