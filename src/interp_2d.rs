use nalgebra::{MatrixN, VectorN, U10};
use std::cell::RefCell;

pub struct Interp2d {
    data: RefCell<Vec<(f64, f64, f64)>>,
}

impl Interp2d {
    pub fn new(data: Vec<(f64, f64, f64)>) -> Interp2d {
        Interp2d {
            data: RefCell::new(data),
        }
    }

    pub fn call(&self, p: (f64, f64)) -> f64 {
        let mut d = self.data.borrow_mut();
        d.sort_by(|a, b| f64::partial_cmp(
            &((a.0 - p.0).powi(2) + (a.1 - p.1).powi(2)),
            &((b.0 - p.0).powi(2) + (b.1 - p.1).powi(2))
        ).expect("ERROR: Problem sorting interpolation data."));

        let p1 = d[0];
        let p2 = d[1];
        let p3 = d[2];
        let p4 = d[3];

        let a = MatrixN::<f64, U10>::from_iterator(vec![
            1.0,         0.0,         0.0,         0.0,  0.0,  0.0, p1.0 * p1.0, p2.0 * p2.0, p3.0 * p3.0, p4.0 * p4.0,
            0.0,         1.0,         0.0,         0.0,  0.0,  0.0, p1.0 * p1.1, p2.0 * p2.1, p3.0 * p3.1, p4.0 * p4.1,
            0.0,         0.0,         1.0,         0.0,  0.0,  0.0, p1.1 * p1.1, p2.1 * p2.1, p3.1 * p3.1, p4.1 * p4.1,
            0.0,         0.0,         0.0,         0.0,  0.0,  0.0, p1.0,        p2.0,        p3.0,        p4.0,
            0.0,         0.0,         0.0,         0.0,  0.0,  0.0, p1.1,        p2.1,        p3.1,        p4.1,
            0.0,         0.0,         0.0,         0.0,  0.0,  0.0, 1.0,         1.0,         1.0,         1.0,
            p1.0 * p1.0, p1.0 * p1.1, p1.1 * p1.1, p1.0, p1.1, 1.0, 0.0,         0.0,         0.0,         0.0,
            p2.0 * p2.0, p2.0 * p2.1, p2.1 * p2.1, p2.0, p2.1, 1.0, 0.0,         0.0,         0.0,         0.0,
            p3.0 * p3.0, p3.0 * p3.1, p3.1 * p3.1, p3.0, p3.1, 1.0, 0.0,         0.0,         0.0,         0.0,
            p4.0 * p4.0, p4.0 * p4.1, p4.1 * p4.1, p4.0, p4.1, 1.0, 0.0,         0.0,         0.0,         0.0
        ].into_iter());

        let b = VectorN::<f64, U10>::from_iterator(vec![
            0.0, 0.0, 0.0, 0.0, 0.0, 0.0, p1.2, p2.2, p3.2, p4.2
        ].into_iter());

        let res = a.lu().solve(&b).expect("ERROR: Problem interpolating in 2D");
        res[0] * p.0 * p.0 + res[1] * p.0 * p.1 + res[2] * p.1 * p.1 + res[3] * p.0 + res[4] * p.1 + res[5]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn interpolate() {
        let v = vec![(0.0, 0.0, 0.0), (1.0, 0.0, 1.0), (0.0, 1.0, 1.0), (1.0, 1.0, 2.0)];
        let i = Interp2d::new(v);
        assert!((i.call((0.5, 0.5)) - 1.0).abs() < ::std::f64::EPSILON);
        assert!((i.call((0.75, 0.75)) - 1.5).abs() < ::std::f64::EPSILON);

        let v = vec![(0.0, 0.0, 0.0), (1.0, 0.0, 1.0), (0.0, 1.0, 1.0), (1.0, 1.0, 2.0), (-100.0, -100.0, 0.0), (-100.0, 100.0, 0.0), (100.0, -100.0, 0.0), (100.0, 100.0, 0.0)];
        let i = Interp2d::new(v);
        assert!((i.call((0.5, 0.5)) - 1.0).abs() < ::std::f64::EPSILON);
    }
}
