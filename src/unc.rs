pub use datakiste::unc::{Unc, ValUnc as DkValUnc};
use std::ops::{Deref, DerefMut};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Default)]
#[repr(transparent)]
pub struct ValUnc(pub val_unc::ValUnc<f64, (Unc, Unc)>);

impl ValUnc {
    pub fn new(val: f64, unc: (Unc, Unc)) -> Self {
        Self(val_unc::ValUnc { val, unc })
    }
}

impl From<DkValUnc> for ValUnc {
    fn from(v: DkValUnc) -> Self {
        Self(val_unc::ValUnc {
            val: v.val,
            unc: (v.unc, Unc(0.0)),
        })
    }
}

impl Deref for ValUnc {
    type Target = val_unc::ValUnc<f64, (Unc, Unc)>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for ValUnc {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
