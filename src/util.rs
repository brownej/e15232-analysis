use crate::{
    analysis::Analysis,
    errors::*,
    packet::SiPacket,
    unc::{DkValUnc, Unc, ValUnc},
};
use datakiste::{self, event::Hit as DkHit, DaqId, DetId};
use std::{convert::TryFrom, f64::consts::FRAC_1_PI};

pub mod output;
pub mod particles;

pub fn is_si_proton(a: &Analysis, p_si: &SiPacket) -> Result<bool> {
    let c_si_de60_e60_p = a
        .get_cut_2d("si_de60_e60_p")
        .chain_err(|| "No telescope proton cut")?;
    let c_si_p_singles = a
        .get_cut_2d("si_p_singles")
        .chain_err(|| "No singles proton cut")?;

    // FIXME: This should check that whether the detector is a single or telescope, but checking
    // for dE and E should be reasonable
    match (p_si.comp_de(), p_si.comp_e()) {
        (Some(ch_de), Some(ch_e)) => {
            let (de60, e60) = a.si_thick_corr(&ch_de, &ch_e)?;
            if c_si_de60_e60_p.contains(e60, de60) {
                Ok(true)
            } else {
                Ok(false)
            }
        }
        (None, Some(ch_e)) => {
            if c_si_p_singles.contains(
                a.get_angle_info(ch_e.detnum, ch_e.detch)?.theta_avg,
                ch_e.energy,
            ) {
                Ok(true)
            } else {
                Ok(false)
            }
        }
        _ => Ok(false),
    }
}

pub fn is_si_alpha(a: &Analysis, p_si: &SiPacket) -> Result<bool> {
    let c_si_de60_e60_a = a.get_cut_2d("si_de60_e60_a").chain_err(|| "No alpha cut")?;
    if let (Some(ch_de), Some(ch_e)) = (p_si.comp_de(), p_si.comp_e()) {
        let (de60, e60) = a.si_thick_corr(&ch_de, &ch_e)?;
        if c_si_de60_e60_a.contains(e60, de60) {
            return Ok(true);
        }
    }
    Ok(false)
}

pub fn is_si_single(p_si: &SiPacket) -> Result<bool> {
    for h in p_si.all_e() {
        match h.detid.0 {
            1..=6 | 8 | 11 => return Ok(true),
            _ => {}
        }
    }
    Ok(false)
}

#[derive(Debug, Clone)]
pub struct Hit {
    pub daqid: DaqId,
    pub detid: DetId,
    pub rawval: u16,
    pub value: u16,
    pub energy: ValUnc,
    pub time: f64,
}

impl TryFrom<DkHit> for Hit {
    type Error = Error;

    fn try_from(h: DkHit) -> Result<Hit> {
        use std::f64::NAN;

        let DkHit {
            daqid,
            detid,
            rawval,
            value,
            energy,
            time,
            ..
        } = h;

        // FIXME: these are kinda cheaty
        let detid = detid.unwrap_or(DetId(0, 0));
        let value = value.unwrap_or(rawval);
        let energy = energy.map_or(ValUnc::new(NAN, (Unc(NAN), Unc(NAN))), ValUnc::from);

        Ok(Hit {
            daqid,
            detid,
            rawval,
            value,
            energy,
            time,
        })
    }
}

impl From<Hit> for DkHit {
    fn from(h: Hit) -> DkHit {
        let Hit {
            daqid,
            detid,
            rawval,
            value,
            energy,
            time,
        } = h;
        let energy = DkValUnc::new(
            energy.val,
            Unc((energy.unc.0).0.abs() + (energy.unc.1).0.abs()),
        );
        DkHit {
            daqid,
            detid: Some(detid),
            rawval,
            value: Some(value),
            energy: Some(energy),
            time,
            trace: Vec::new(),
        }
    }
}

impl From<&Hit> for DkHit {
    fn from(h: &Hit) -> DkHit {
        /*
        let Hit {
            daqid,
            detid,
            rawval,
            value,
            energy,
            time,
        } = *h;
        let energy = ValUnc {
            val: energy.val,
            unc: energy.sys + energy.stat,
        };
        DkHit {
            daqid,
            detid: Some(detid),
            rawval,
            value: Some(value),
            energy: Some(energy),
            time,
            trace: Vec::new(),
        }
        */
        //(*h as Hit).into()
        h.into()
    }
}

pub fn ic_angles(ic_pos_x: f64, ic_pos_y: f64) -> (f64, f64) {
    let ic_dist_z_x = 558.8 + 20.0 + 18.3;
    let ic_dist_z_y = ic_dist_z_x + 36.6;
    let ic_pos_center_x = 16.597;
    let ic_pos_center_y = 15.000;

    let ic_dist_x = (ic_pos_x - ic_pos_center_x) * 3.0;
    let ic_dist_y = (ic_pos_y - ic_pos_center_y) * 3.0;

    let ic_theta = 180.
        * FRAC_1_PI
        * f64::atan(f64::sqrt(
            (ic_dist_x * ic_dist_x) / (ic_dist_z_x * ic_dist_z_x)
                + (ic_dist_y * ic_dist_y) / (ic_dist_z_y * ic_dist_z_y),
        ));
    let ic_phi = 180. * FRAC_1_PI * f64::atan2(-ic_dist_y, ic_dist_x);

    (ic_theta, ic_phi)
}
