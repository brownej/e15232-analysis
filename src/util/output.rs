use crate::util::particles::{Ejectile, Recoil};
use chrono::{DateTime, Utc};

pub mod format;
pub use format::{print_formatted, write_formatted};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Output {
    pub info: Info,
    pub data: Vec<Data>,
}

impl Output {
    pub fn new() -> Self {
        Self {
            info: Info::new(),
            data: Vec::new(),
        }
    }

    pub fn push_data(&mut self, new_data: Data) {
        self.data.push(new_data)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Info {
    pub time: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<String>,
}

impl Info {
    pub fn new() -> Self {
        Self {
            time: Utc::now(),
            comment: None,
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Data {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub run_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub run_pressure: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub event_id: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ejectile_type: Option<Ejectile>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub recoil_type: Option<Recoil>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_x_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_x_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_y_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_y_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_de_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_de_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_e_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_e_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_tot_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_pos_x: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_pos_y: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_theta: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_phi: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_de_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_de_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_e_detid: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_e_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_tot_energy: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_detno: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_theta: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub si_phi: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub q_value: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub theta_cm: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_ic_34s: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_ic_34cl: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_ic_34ar: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_si_34s: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_si_34cl: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prob_si_34ar: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_x_chan: Option<Vec<(u16, f64)>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_y_chan: Option<Vec<(u16, f64)>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_de_chan: Option<Vec<f64>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub ic_e_chan: Option<Vec<f64>>,
}

impl Data {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn run_name<T>(mut self, run_name: T) -> Self
    where
        T: Into<Option<String>>,
    {
        self.run_name = run_name.into();
        self
    }

    pub fn run_pressure<T>(mut self, run_pressure: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.run_pressure = run_pressure.into();
        self
    }

    pub fn event_id<T>(mut self, event_id: T) -> Self
    where
        T: Into<Option<u64>>,
    {
        self.event_id = event_id.into();
        self
    }

    pub fn ejectile_type<T>(mut self, ejectile_type: T) -> Self
    where
        T: Into<Option<Ejectile>>,
    {
        self.ejectile_type = ejectile_type.into();
        self
    }

    pub fn recoil_type<T>(mut self, recoil_type: T) -> Self
    where
        T: Into<Option<Recoil>>,
    {
        self.recoil_type = recoil_type.into();
        self
    }

    pub fn ic_x_detid<T>(mut self, ic_x_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.ic_x_detid = ic_x_detid.into();
        self
    }

    pub fn ic_x_energy<T>(mut self, ic_x_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_x_energy = ic_x_energy.into();
        self
    }

    pub fn ic_y_detid<T>(mut self, ic_y_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.ic_y_detid = ic_y_detid.into();
        self
    }

    pub fn ic_y_energy<T>(mut self, ic_y_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_y_energy = ic_y_energy.into();
        self
    }

    pub fn ic_de_detid<T>(mut self, ic_de_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.ic_de_detid = ic_de_detid.into();
        self
    }

    pub fn ic_de_energy<T>(mut self, ic_de_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_de_energy = ic_de_energy.into();
        self
    }

    pub fn ic_e_detid<T>(mut self, ic_e_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.ic_e_detid = ic_e_detid.into();
        self
    }

    pub fn ic_e_energy<T>(mut self, ic_e_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_e_energy = ic_e_energy.into();
        self
    }

    pub fn ic_tot_energy<T>(mut self, ic_tot_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_tot_energy = ic_tot_energy.into();
        self
    }

    pub fn ic_pos_x<T>(mut self, ic_pos_x: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_pos_x = ic_pos_x.into();
        self
    }

    pub fn ic_pos_y<T>(mut self, ic_pos_y: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_pos_y = ic_pos_y.into();
        self
    }

    pub fn ic_theta<T>(mut self, ic_theta: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_theta = ic_theta.into();
        self
    }

    pub fn ic_phi<T>(mut self, ic_phi: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.ic_phi = ic_phi.into();
        self
    }

    pub fn si_de_detid<T>(mut self, si_de_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.si_de_detid = si_de_detid.into();
        self
    }

    pub fn si_de_energy<T>(mut self, si_de_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.si_de_energy = si_de_energy.into();
        self
    }

    pub fn si_e_detid<T>(mut self, si_e_detid: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.si_e_detid = si_e_detid.into();
        self
    }

    pub fn si_e_energy<T>(mut self, si_e_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.si_e_energy = si_e_energy.into();
        self
    }

    pub fn si_tot_energy<T>(mut self, si_tot_energy: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.si_tot_energy = si_tot_energy.into();
        self
    }

    pub fn si_detno<T>(mut self, si_detno: T) -> Self
    where
        T: Into<Option<u16>>,
    {
        self.si_detno = si_detno.into();
        self
    }

    pub fn si_theta<T>(mut self, si_theta: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.si_theta = si_theta.into();
        self
    }

    pub fn si_phi<T>(mut self, si_phi: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.si_phi = si_phi.into();
        self
    }

    pub fn q_value<T>(mut self, q_value: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.q_value = q_value.into();
        self
    }

    pub fn theta_cm<T>(mut self, theta_cm: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.theta_cm = theta_cm.into();
        self
    }

    pub fn prob_ic_34s<T>(mut self, prob_ic_34s: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_ic_34s = prob_ic_34s.into();
        self
    }

    pub fn prob_ic_34cl<T>(mut self, prob_ic_34cl: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_ic_34cl = prob_ic_34cl.into();
        self
    }

    pub fn prob_ic_34ar<T>(mut self, prob_ic_34ar: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_ic_34ar = prob_ic_34ar.into();
        self
    }

    pub fn prob_si_34s<T>(mut self, prob_si_34s: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_si_34s = prob_si_34s.into();
        self
    }

    pub fn prob_si_34cl<T>(mut self, prob_si_34cl: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_si_34cl = prob_si_34cl.into();
        self
    }

    pub fn prob_si_34ar<T>(mut self, prob_si_34ar: T) -> Self
    where
        T: Into<Option<f64>>,
    {
        self.prob_si_34ar = prob_si_34ar.into();
        self
    }

    pub fn ic_x_chan<T>(mut self, ic_x_chan: T) -> Self
    where
        T: Into<Option<Vec<(u16, f64)>>>,
    {
        self.ic_x_chan = ic_x_chan.into();
        self
    }

    pub fn ic_y_chan<T>(mut self, ic_y_chan: T) -> Self
    where
        T: Into<Option<Vec<(u16, f64)>>>,
    {
        self.ic_y_chan = ic_y_chan.into();
        self
    }

    pub fn ic_de_chan<T>(mut self, ic_de_chan: T) -> Self
    where
        T: Into<Option<Vec<f64>>>,
    {
        self.ic_de_chan = ic_de_chan.into();
        self
    }

    pub fn ic_e_chan<T>(mut self, ic_e_chan: T) -> Self
    where
        T: Into<Option<Vec<f64>>>,
    {
        self.ic_e_chan = ic_e_chan.into();
        self
    }
}
