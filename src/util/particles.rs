#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Ejectile {
    #[serde(rename = "a")]
    Alpha,
    #[serde(rename = "p")]
    Proton,
    #[serde(rename = "?")]
    Unknown,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Recoil {
    #[serde(rename = "34s")]
    S34,
    #[serde(rename = "34cl")]
    Cl34,
    #[serde(rename = "34ar")]
    Ar34,
    #[serde(rename = "37cl")]
    Cl37,
    #[serde(rename = "37ar")]
    Ar37,
    #[serde(rename = "37k")]
    K37,
    #[serde(rename = "?")]
    Unknown,
}
