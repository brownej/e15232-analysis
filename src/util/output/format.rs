use serde::ser::Serialize;
use std::io::{self, stdout, Write};

pub struct MyFormatter {
    current_indent: usize,
    max_indent: usize,
    has_value: bool,
}

impl MyFormatter {
    pub fn new() -> Self {
        Self {
            current_indent: 0,
            max_indent: 3,
            has_value: false,
        }
    }
}

impl serde_json::ser::Formatter for MyFormatter {
    #[inline]
    fn begin_array<W>(&mut self, writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.current_indent += 1;
        self.has_value = false;
        writer.write_all(b"[")
    }

    #[inline]
    fn end_array<W>(&mut self, writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.current_indent -= 1;

        if self.has_value {
            writer.write_all(b"\n")?;
            indent(writer, self.current_indent, b"  ")?;
        }

        writer.write_all(b"]")
    }

    #[inline]
    fn begin_array_value<W>(&mut self, writer: &mut W, first: bool) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        if first {
            writer.write_all(b"\n")?;
        } else {
            writer.write_all(b",\n")?;
        }
        indent(writer, self.current_indent, b"  ")?;
        Ok(())
    }

    #[inline]
    fn end_array_value<W>(&mut self, _writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.has_value = true;
        Ok(())
    }

    #[inline]
    fn begin_object<W>(&mut self, writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.current_indent += 1;
        self.has_value = false;
        writer.write_all(b"{")
    }

    #[inline]
    fn end_object<W>(&mut self, writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.current_indent -= 1;

        if self.current_indent + 1 < self.max_indent {
            if self.has_value {
                writer.write_all(b"\n")?;
                indent(writer, self.current_indent, b"  ")?;
            }
        } else {
            if self.has_value {
                writer.write_all(b" ")?;
            }
        }

        writer.write_all(b"}")
    }

    #[inline]
    fn begin_object_key<W>(&mut self, writer: &mut W, first: bool) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        if !first {
            writer.write_all(b",")?;
        }
        if self.current_indent < self.max_indent {
            writer.write_all(b"\n")?;
            indent(writer, self.current_indent, b"  ")?
        } else {
            writer.write_all(b" ")?;
        }
        Ok(())
    }

    #[inline]
    fn begin_object_value<W>(&mut self, writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        writer.write_all(b": ")
    }

    #[inline]
    fn end_object_value<W>(&mut self, _writer: &mut W) -> io::Result<()>
    where
        W: ?Sized + io::Write,
    {
        self.has_value = true;
        Ok(())
    }
}

fn indent<W>(wr: &mut W, n: usize, s: &[u8]) -> io::Result<()>
where
    W: ?Sized + io::Write,
{
    for _ in 0..n {
        wr.write_all(s)?;
    }

    Ok(())
}

pub fn print_formatted<T: ?Sized>(value: &T) -> io::Result<()>
where
    T: Serialize,
{
    let stdout = stdout();
    let stdout = stdout.lock();
    write_formatted(stdout, value)
}

pub fn write_formatted<W, T: ?Sized>(writer: W, value: &T) -> io::Result<()>
where
    W: Write,
    T: Serialize,
{
    let mut s = serde_json::Serializer::with_formatter(writer, MyFormatter::new());
    value.serialize(&mut s)?;
    Ok(())
}
